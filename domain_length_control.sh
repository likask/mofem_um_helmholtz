#!/bin/sh
BEST_NB_PRC="8"
# " chmod -x FILE_NAME " before run the bach script
DIR_NAME="error_mesh_file"
NB_PROC="8"
ERROR_TYPE="l2"  #change the error type here to h1
WAVE_NUMBER="0"
ODISP="1"
ORDER="adaptivity"

# for WAVE_NUMBER in {3,5,10}
# do
partition="false"
DUFFY="true"
LOBATTO="true"
ERRORLV="0"
PRIORI_TYPE="1"
# file1="impinging_cylinder_$ODISP.cub"
# file2="analytical_solution.h5m"
#ERROR_TYPE="h1"

# BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
# SRC_PATH="@CMAKE_CURRENT_SOURCE_DIR@"
# BIN_PATH="./"
# SRC_PATH="./src"

# MPIRUN="mpirun -np $NB_PROC"

# cd $BIN_PATH



# echo LOG: plane wave guide P convergence test
# echo | tee -a wave_guide_p.txt
# echo "Start p convergence test for plane wave guide ..." | tee -a wave_guide_p.txt
# $MPIRUN $BIN_PATH/best_approximation \
# -my_file plane_wave_cube.cub \
# -my_is_partitioned false \
# -wave_number $WAVE_NUMBER \
# -wave_direction 0.7071,0.7071,0 \
# -analytical_solution_type plane_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package mumps \
# -ksp_monitor \
# -my_order 7 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -wave_guide_angle 45
#
# for ODISP in {1..3..1};
# do
#   let NB_PROC=${ODISP}+1
#   #echo $NB_PROC
#   #BEGIN analytical solution
#   # $MPIRUN $BIN_PATH/best_approximation \
#   # -my_file plane_wave_cube.cub \
#   # -my_is_partitioned false \
#   # -wave_number $WAVE_NUMBER \
#   # -wave_direction 0.7071,0.7071,0 \
#   # -analytical_solution_type plane_wave \
#   # -save_postproc_mesh false \
#   # -ksp_type fgmres \
#   # -pc_type lu \
#   # -pc_factor_mat_solver_package mumps \
#   # -ksp_monitor \
#   # -my_order $ODISP \
#   # -my_max_post_proc_ref_level 0 \
#   # -add_incident_wave false \
#   # -wave_guide_angle 45
#
#   #BEGIN numerical solution
#   $MPIRUN $BIN_PATH/fe_approximation \
#   -my_file analytical_solution.h5m \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 0.7071,0.7071,0 \
#   -analytical_solution_type plane_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order $ODISP \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -wave_guide_angle 45 |
#   grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =" | tee -a -i wave_guide_p.txt
#   #BEGIN error calculation
#
#   $MPIRUN $BIN_PATH/error_norm \
#   -my_file $BIN_PATH/fe_solution.h5m \
#   -norm_type $ERROR_TYPE \
#   -relative_error false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps  \
#   -ksp_monitor  \
#   -my_order 1 \
#   -my_max_post_proc_ref_level 0 \
#   -save_postproc_mesh true \
#   2>&1 | grep --line-buffered -i " $ERROR_TYPE realtive error " | tee -a -i wave_guide_p.txt
#   #RENAME files inot their order and wave number.
#   mbconvert norm_error.h5m norm_error.vtk
#   mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ODISP}.vtk
# done
#
# #SAVE the results to different directory
# mkdir ${DIR_NAME}_${ERROR_TYPE}_waveguide | mv norm_error_k${WAVE_NUMBER}_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_waveguide
rm -rf test_best_approximation_k_*_errorlv_*.txt
rm -rf ${DIR_NAME}_${ERROR_TYPE}_cylinder_p
rm -rf error_hard_cylinder_k_*_lobatto_${LOBATTO}_errorlv_*_type_*.txt
rm -rf hard_cylinder_k_*_lobatto_${LOBATTO}_errorlv_*_type_*.txt | rm -rf norm_error_k*_order_*.vtk


# for batman in `seq 1 1`;
# do

  #echo $NB_PROC
  #BEGIN analytical solution
# mpirun -np 8 ./best_approximation \
#   -my_file impinging_cylinder.cub \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 1,0,0 \
#   -analytical_solution_type hard_cylinder_incident_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order 7 \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -amplitude_of_incident_wave 1
# done

# for ODISP in `seq 1 4`;

#Frequencies : 1 5 10 20 30 40 50 70
for ERROR_TYPE in "h1" "l2"
do

if [ $ERROR_TYPE = "h1" ]
then PRIORI_TYPE="2"
fi


# for WAVE_NUMBER in "0.3142" "1.5708" "3.1416" "6.2832" "9.4248" "12.5664" "15.7080"
# 1 5 10 30 50 70
# "3" "6" "8" fails
# for WAVE_NUMBER in "2" "4" "5" "7" "10"
for WAVE_NUMBER in "1" "3" "6" "8" "15" "20"
do


if [ "$WAVE_NUMBER" -gt "6" ]
then NB_PROC=`expr 8`
else NB_PROC=`expr 4`
fi
# echo NB_PROC = $NB_PROC




for ERRORLV in "0" "1" "2"
do


# echo LOG: sound hard cylinder P convergence test
echo | tee -a test_best_approximation_k_${WAVE_NUMBER}_errorlv_${ERRORLV}.txt
echo | tee -a hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo | tee -a error_hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo " avg nze max min DOFs memory unit time CPUtime              "| tee -a hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "${ERROR_TYPE}relative error                "| tee -a error_hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "Start p convergence test for sound hard cylinder ..."

# if [ $partition = true ]; then mbpart -t -p PARTKWAY $NB_PROC  impinging_cylinder.cub  impinging_cylinder_parts$NB_PROC.h5m; fi

# if [ $partition = true ]
# then file1="impinging_cylinder_parts$NB_PROC.h5m" && file2="best_solution.h5m"
# fi

for ODISP in "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12"
do

# BEST_NB_PRC=`expr 2 + $ODISP`

echo "Start domain length analysis best_approximation calculation with $BEST_NB_PRC processes, mesh h$ODISP and order 8 and wavenumber $WAVE_NUMBER, error level $ERRORLV"

#BEGIN analytical solution
mpirun -np $BEST_NB_PRC ./best_approximation \
-my_file impinging_cylinder_domain_$ODISP.cub \
-my_is_partitioned false \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-analytical_solution_type hard_cylinder_scatter_wave \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 8 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave false \
-lobatto true 2>&1 |
echo $(grep --line-buffered -i "Residual norm\|Time =" | sort | uniq) | tee -a -i test_best_approximation_k_${WAVE_NUMBER}_errorlv_${ERRORLV}.txt


# for ORDER in {1..7..1};
# for ORDER in `seq 1 8`;
# do
 # let NB_PROC+=${ORDER}
 # NB_PROC=`expr $ORDER + 1`
# if [ "$ORDER" -gt "$ODISP" ]
# then NB_PROC=`expr $ORDER + 1`
# else NB_PROC=`expr $ODISP + 1`
# fi
# $(echo $(( 2 * $ODISP )))
 # NB_PROC=`expr $ODISP + $(echo $(( 2 * $ERRORLV )))`
 MPIRUN="mpirun -np $NB_PROC"


 #To use partitioned mesh, first mbpart -t -p PARTKWAY 4  cylinder4.cub  cylinder4_4parts.h5m,
#   let NB_PROC=${ORDER}+1
#   #echo $NB_PROC
  # #BEGIN analytical solution
  # $MPIRUN $BIN_PATH/best_approximation \
  # -my_file $file1 \
  # -my_is_partitioned $partition \
  # -wave_number $WAVE_NUMBER \
  # -wave_direction 1,0,0 \
  # -analytical_solution_type hard_cylinder_incident_wave \
  # -save_postproc_mesh false \
  # -ksp_type fgmres \
  # -pc_type lu \
  # -pc_factor_mat_solver_package mumps \
  # -ksp_monitor \
  # -my_order $ORDER \
  # -my_max_post_proc_ref_level 0 \
  # -add_incident_wave false \

#
#BEGIN numerical solution
#echo "                      "| tee -a hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo "${ERROR_TYPE}relative error                "| tee -a error_hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo "                                   "| tee -a hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo "Start numerical calculation with $NB_PROC processes, mesh h$ODISP and order $ORDER ..." | tee -a hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "Start domain length analysis numerical calculation with $NB_PROC processes, mesh h$ODISP and order $ORDER and wavenumber $WAVE_NUMBER, error level $ERRORLV"
mpirun -np $NB_PROC ./fe_approximation \
-my_file analytical_solution.h5m \
-my_is_partitioned false \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 1 \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy $DUFFY \
-lobatto $LOBATTO \
-adaptivity true \
-error_level $ERRORLV \
-error_type $PRIORI_TYPE \
-add_incident_wave false 2>&1 |
echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$4,$6,$8,$13,$23,$30,$35}' | tee -a -i hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "   "
echo "                       begin error calculation           "
echo "   "
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$25,$29,$30}' | tee -a -i hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#BEGIN error calculation   awk '{print $6,$12}'  grep first 10 lines |head -10|
#grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt

mpirun -np 2 ./error_norm \
-my_file ./fe_solution.h5m \
-my_is_partitioned false \
-norm_type $ERROR_TYPE \
-relative_error false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 1 \
-my_max_post_proc_ref_level 0 \
-save_postproc_mesh true 2>&1 | echo $(grep --line-buffered -i " realtive error ") | awk '{print $6,$12}' | tee -a -i error_hard_cylinder_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#RENAME files inot their order and wave number.
mbconvert norm_error.h5m norm_error.vtk |
mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ORDER}.vtk
done

done

done

done
#SAVE the results to different directory
mkdir ${DIR_NAME}_${ERROR_TYPE}_cylinder_p
mv norm_error_k*_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_cylinder_p


# done
#foo="Hello"
#foo="$foo World"
#echo $foo
#> Hello World
