# analysis of automatic adapptivity performed on droplet application */


rm -rf droplet_adaptivity_pressure
rm -rf droplet_reference_pressure
rm -rf droplet_h_refinment_pressure

# # remove all the previous time dependant results.

mkdir droplet_adaptivity_pressure
mkdir droplet_reference_pressure
mkdir droplet_h_refinment_pressure

# COMPUTE THE AUTOMATIC ADAPTIVITY SCHEME

# rm -rf droplet_pressure
#
#
~/build_moFEM1/usr/tools/mofem_part -my_file ./coarse_droplet.cub -my_nparts 8
#
# # 4 MHz
mpirun -np 8 ./fe_field_split \
-my_file out.h5m \
-my_is_partitioned true \
-duffy true \
-adaptivity true \
-error_level 1 \
-my_order 6 \
-save_postproc_mesh true \
-wave_number 6283.1 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave true \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,0.000000001 \
-monochromatic_wave true \
-signal_length 0.001 \
-radiation_field true \
-frequency 4000000 \
-signal_duration 0.00000025 \
-velocity 1500 \
-complex_wave_number 79.8387 \
-transmission_coefficient 0.1491 \
-rayleigh_wave false \
-density 998 \
-reynolds_stress false \
-radiation_force false \
-material_coefficient1 -1348.8  \
-space_data acoustic_data.txt \
-nb_of_time_step 14 \
-material_coefficient2  \
-fieldsplit_1_ksp_type fgmres \
-fieldsplit_1_pc_type lu \
-fieldsplit_1_pc_factor_mat_solver_package superlu_dist \
-fieldsplit_1_ksp_max_it 25 \
-fieldsplit_0_ksp_type fgmres \
-fieldsplit_0_pc_type lu \
-fieldsplit_0_pc_factor_mat_solver_package superlu_dist \
-fieldsplit_0_ksp_max_it 25 \
-pc_fieldsplit_type schur \
-ksp_type fgmres \
-ksp_atol 1e-8 \
-ksp_rtol 1e-8 \
-ksp_max_it 800 \
-ksp_monitor \
-ksp_converged_reason \
-droplet_radius 0.00210 \
-attenuation 0.067077277922418 | tee log_droplet_adaptivity_4MHZ



mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mbconvert fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_4MHZ.vtk
mv fe_solution_mesh_post_proc_4MHZ.vtk droplet_adaptivity_pressure
mv log_droplet_adaptivity_4MHZ droplet_adaptivity_pressure

# -complex_wave_number 2768 \

#
mpirun -np 8 ./fe_field_split \
-my_file out.h5m \
-my_is_partitioned true \
-ksp_final_residual \
-ksp_monitor \
-ksp_converged_reason \
-pc_type lu \
-ksp_type fgmres \
-pc_factor_mat_solver_package superlu_dist \
-duffy true \
-adaptivity true \
-error_level 1 \
-my_order 6 \
-save_postproc_mesh true \
-wave_number 15707.96326794897 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave true \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-monochromatic_wave true \
-signal_length 0.0004 \
-signal_duration 0.0000001 \
-frequency 10000000 \
-velocity 1500 \
-complex_wave_number 199.5968 \
-transmission_coefficient 0.1491 \
-rayleigh_wave false \
-density 998 \
-droplet_radius 0.00210 \
-attenuation 0.265146221848709 \
-material_coefficient1 -1348.8 \
-ksp_atol 1e-8 \
-ksp_rtol 1e-8 \
-ksp_max_it 2000 \
-space_data acoustic_data.txt \
-nb_of_time_step 13 \
-material_coefficient2 | tee log_droplet_adaptivity_10MHZ

mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mbconvert fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_10MHZ.vtk
mv fe_solution_mesh_post_proc_10MHZ.vtk droplet_adaptivity_pressure
mv log_droplet_adaptivity_10MHZ droplet_adaptivity_pressure



mpirun -np 8 ./fe_field_split \
-my_file out.h5m \
-my_is_partitioned true \
-ksp_final_residual \
-ksp_monitor \
-ksp_converged_reason \
-pc_type lu \
-ksp_type fgmres \
-pc_factor_mat_solver_package superlu_dist \
-duffy true \
-adaptivity true \
-error_level 1 \
-my_order 6 \
-save_postproc_mesh true \
-wave_number 31415.92653589793 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave true \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-monochromatic_wave true \
-signal_length 0.0002 \
-signal_duration 0.00000005 \
-frequency 20000000 \
-velocity 1500 \
-complex_wave_number 399.1935 \
-transmission_coefficient 0.1491 \
-rayleigh_wave false \
-density 998 \
-droplet_radius 0.00210 \
-attenuation 0.749946765900858 \
-material_coefficient1 -1348.8 \
-ksp_atol 1e-8 \
-ksp_rtol 1e-8 \
-ksp_max_it 2000 \
-space_data acoustic_data.txt \
-nb_of_time_step 13 \
-material_coefficient2 | tee log_droplet_adaptivity_20MHZ

mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mbconvert fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_20MHZ.vtk
mv fe_solution_mesh_post_proc_20MHZ.vtk droplet_adaptivity_pressure
mv log_droplet_adaptivity_20MHZ droplet_adaptivity_pressure

#
# ./do_vtk1.sh pressure_real_time_step_*.h5m
# mbconvert reynolds_stress.h5m reynolds_stress.vtk
# mkdir droplet_pressure
# mv pressure_real_time_step_*.vtk droplet_pressure
# mv reynolds_stress.vtk droplet_pressure


# -complex_wave_number 199.5968 \


##################################################################################################################################

##################################################################################################################################

##################################################################################################################################

##################################################################################################################################

##################################################################################################################################



# COMPUTE THE hhhhhhhhh ADAPTIVITY SCHEME

# rm -rf droplet_pressure
#
#
~/build_moFEM1/usr/tools/mofem_part -my_file ./dense_droplet.cub -my_nparts 8
#
# # 4 MHz
mpirun -np 8 ./fe_field_split \
-my_file out.h5m \
-my_is_partitioned true \
-duffy true \
-adaptivity false \
-error_level 1 \
-my_order 2 \
-save_postproc_mesh true \
-wave_number 6283.1 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave true \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,0.000000001 \
-monochromatic_wave true \
-signal_length 0.001 \
-radiation_field true \
-frequency 4000000 \
-signal_duration 0.00000025 \
-velocity 1500 \
-complex_wave_number 79.8387 \
-transmission_coefficient 0.1491 \
-rayleigh_wave false \
-density 998 \
-reynolds_stress false \
-radiation_force false \
-material_coefficient1 -1348.8  \
-ksp_atol 1e-8 \
-ksp_rtol 1e-8 \
-ksp_max_it 2000 \
-ksp_monitor \
-space_data acoustic_data.txt \
-nb_of_time_step 14 \
-material_coefficient2  \
-ksp_converged_reason \
-ksp_monitor_true_residual \
-pc_type lu \
-ksp_type fgmres \
-pc_factor_mat_solver_package superlu_dist \
-droplet_radius 0.00210 \
-attenuation 0.067077277922418 | tee log_droplet_h_refinment_4MHZ



mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mbconvert fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_4MHZ.vtk
mv fe_solution_mesh_post_proc_4MHZ.vtk droplet_h_refinment_pressure
mv log_droplet_h_refinment_4MHZ droplet_h_refinment_pressure

# -complex_wave_number 2768 \

#
mpirun -np 8 ./fe_field_split \
-my_file out.h5m \
-my_is_partitioned true \
-ksp_final_residual \
-ksp_monitor \
-ksp_converged_reason \
-pc_type lu \
-ksp_type fgmres \
-pc_factor_mat_solver_package superlu_dist \
-duffy true \
-adaptivity false \
-error_level 1 \
-my_order 2 \
-save_postproc_mesh true \
-wave_number 15707.96326794897 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave true \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-monochromatic_wave true \
-signal_length 0.0004 \
-signal_duration 0.0000001 \
-frequency 10000000 \
-velocity 1500 \
-complex_wave_number 199.5968 \
-transmission_coefficient 0.1491 \
-rayleigh_wave false \
-density 998 \
-droplet_radius 0.00210 \
-attenuation 0.265146221848709 \
-material_coefficient1 -1348.8 \
-ksp_atol 1e-8 \
-ksp_rtol 1e-8 \
-ksp_max_it 2000 \
-space_data acoustic_data.txt \
-nb_of_time_step 13 \
-material_coefficient2 | tee log_droplet_h_refinment_10MHZ

mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mbconvert fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_10MHZ.vtk
mv fe_solution_mesh_post_proc_10MHZ.vtk droplet_h_refinment_pressure
mv log_droplet_h_refinment_10MHZ droplet_h_refinment_pressure



mpirun -np 8 ./fe_field_split \
-my_file out.h5m \
-my_is_partitioned true \
-ksp_final_residual \
-ksp_monitor \
-ksp_converged_reason \
-pc_type lu \
-ksp_type fgmres \
-pc_factor_mat_solver_package superlu_dist \
-duffy true \
-adaptivity false \
-error_level 1 \
-my_order 2 \
-save_postproc_mesh true \
-wave_number 31415.92653589793 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave true \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-monochromatic_wave true \
-signal_length 0.0002 \
-signal_duration 0.00000005 \
-frequency 20000000 \
-velocity 1500 \
-complex_wave_number 399.1935 \
-transmission_coefficient 0.1491 \
-rayleigh_wave false \
-density 998 \
-droplet_radius 0.00210 \
-attenuation 0.749946765900858 \
-material_coefficient1 -1348.8 \
-ksp_atol 1e-8 \
-ksp_rtol 1e-8 \
-ksp_max_it 2000 \
-space_data acoustic_data.txt \
-nb_of_time_step 13 \
-material_coefficient2 | tee log_droplet_h_refinment_20MHZ

mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mbconvert fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_20MHZ.vtk
mv fe_solution_mesh_post_proc_20MHZ.vtk droplet_h_refinment_pressure
mv log_droplet_h_refinment_20MHZ droplet_h_refinment_pressure

##################################################################################################################################

##################################################################################################################################

##################################################################################################################################

##################################################################################################################################

##################################################################################################################################

########################################## Reference solution - Pure Uniform p adaptivity ############################################


#
# ./do_vtk1.sh pressure_real_time_step_*.h5m
# mbconvert reynolds_stress.h5m reynolds_stress.vtk
# mkdir droplet_pressure
# mv pressure_real_time_step_*.vtk droplet_pressure
# mv reynolds_stress.vtk droplet_pressure


# -complex_wave_number 199.5968 \
