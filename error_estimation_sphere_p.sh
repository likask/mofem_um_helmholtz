#!/bin/sh
# " chmod -x FILE_NAME " before run the bach script
DIR_NAME="error_mesh_file"
NB_PROC=1
ERROR_TYPE="l2"
WAVE_NUMBER="3"

# for WAVE_NUMBER in {3,5,10}
# do
partition="false"
DUFFY="true"
LABATTO="false"

file1="impinging_sphere.cub"
file2="analytical_solution.h5m"
#ERROR_TYPE="h1"

# BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
# SRC_PATH="@CMAKE_CURRENT_SOURCE_DIR@"
BIN_PATH="./"
SRC_PATH="./src"

MPIRUN="mpirun -np $NB_PROC"

cd $BIN_PATH

for WAVE_NUMBER in 3 5 10
do

# echo LOG: plane wave guide P convergence test
# echo | tee -a wave_guide_p.txt
# echo "Start p convergence test for plane wave guide ..." | tee -a wave_guide_p.txt
# $MPIRUN $BIN_PATH/best_approximation \
# -my_file plane_wave_cube.cub \
# -my_is_partitioned false \
# -wave_number $WAVE_NUMBER \
# -wave_direction 0.7071,0.7071,0 \
# -analytical_solution_type plane_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 7 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -wave_guide_angle 45
#
# for ODISP in {1..3..1};
# do
#   let NB_PROC=${ODISP}+1
#   #echo $NB_PROC
#   #BEGIN analytical solution
#   # $MPIRUN $BIN_PATH/best_approximation \
#   # -my_file plane_wave_cube.cub \
#   # -my_is_partitioned false \
#   # -wave_number $WAVE_NUMBER \
#   # -wave_direction 0.7071,0.7071,0 \
#   # -analytical_solution_type plane_wave \
#   # -save_postproc_mesh false \
#   # -ksp_type fgmres \
#   # -pc_type lu \
#   # -pc_factor_mat_solver_package superlu_dist \
#   # -ksp_monitor \
#   # -my_order $ODISP \
#   # -my_max_post_proc_ref_level 0 \
#   # -add_incident_wave false \
#   # -wave_guide_angle 45
#
#   #BEGIN numerical solution
#   $MPIRUN $BIN_PATH/fe_approximation \
#   -my_file analytical_solution.h5m \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 0.7071,0.7071,0 \
#   -analytical_solution_type plane_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order $ODISP \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -wave_guide_angle 45 |
#   grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =" | tee -a -i wave_guide_p.txt
#   #BEGIN error calculation
#
#   $MPIRUN $BIN_PATH/error_norm \
#   -my_file $BIN_PATH/fe_solution.h5m \
#   -norm_type $ERROR_TYPE \
#   -relative_error false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package superlu_dist  \
#   -ksp_monitor  \
#   -my_order 1 \
#   -my_max_post_proc_ref_level 0 \
#   -save_postproc_mesh true \
#   2>&1 | grep --line-buffered -i " $ERROR_TYPE realtive error " | tee -a -i wave_guide_p.txt
#   #RENAME files inot their order and wave number.
#   mbconvert norm_error.h5m norm_error.vtk
#   mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ODISP}.vtk
# done
#
# #SAVE the results to different directory
# mkdir ${DIR_NAME}_${ERROR_TYPE}_waveguide | mv norm_error_k${WAVE_NUMBER}_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_waveguide
rm -rf ${DIR_NAME}_${ERROR_TYPE}_sphere_k${WAVE_NUMBER}_p
rm -rf hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt | rm -rf norm_error_k${WAVE_NUMBER}_order_*.vtk
echo LOG: sound hard sphere P convergence test
echo | tee -a hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt
echo "Start p convergence test for sound hard sphere ..." | tee -a hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt

# for batman in `seq 1 1`;
# do

  #echo $NB_PROC
  #BEGIN analytical solution
# mpirun -np 8 ./best_approximation \
#   -my_file impinging_sphere.cub \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 1,0,0 \
#   -analytical_solution_type hard_sphere_incident_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package superlu_dist \
#   -ksp_monitor \
#   -my_order 7 \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -amplitude_of_incident_wave 1
# done

#BEGIN analytical solution
mpirun -np 6 $BIN_PATH/best_approximation \
-my_file $file1 \
-my_is_partitioned $partition \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-analytical_solution_type hard_sphere_incident_wave \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 6 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave false \
-labatto true \

# for ODISP in {1..7..1};
for ODISP in `seq 1 6`;
do
 # let NB_PROC+=${ODISP}
 # NB_PROC=`expr $ODISP + 1`
 #NB_PROC=`expr $ODISP + 1`
 NB_PROC=$ODISP
 MPIRUN="mpirun -np $NB_PROC"
 if [ $partition = true ]; then mbpart -t -p PARTKWAY $NB_PROC  impinging_sphere.cub  impinging_sphere_parts$NB_PROC.h5m; fi

if [ $partition = true ]
then file1="impinging_sphere_parts$NB_PROC.h5m" && file2="best_solution.h5m"
fi

 #To use partitioned mesh, first mbpart -t -p PARTKWAY 4  sphere4.cub  sphere4_4parts.h5m,
#   let NB_PROC=${ODISP}+1
#   #echo $NB_PROC
  # #BEGIN analytical solution
  # $MPIRUN $BIN_PATH/best_approximation \
  # -my_file $file1 \
  # -my_is_partitioned $partition \
  # -wave_number $WAVE_NUMBER \
  # -wave_direction 1,0,0 \
  # -analytical_solution_type hard_sphere_incident_wave \
  # -save_postproc_mesh false \
  # -ksp_type fgmres \
  # -pc_type lu \
  # -pc_factor_mat_solver_package superlu_dist \
  # -ksp_monitor \
  # -my_order $ODISP \
  # -my_max_post_proc_ref_level 0 \
  # -add_incident_wave false \

#
#BEGIN numerical solution
echo "                                   "| tee -a hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt
echo "                                   "| tee -a hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt
echo "Start numerical calculation with $NB_PROC processes, and order $ODISP ..." | tee -a hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt

$MPIRUN $BIN_PATH/fe_approximation \
-my_file $file2 \
-my_is_partitioned $partition \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order $ODISP \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy $DUFFY \
-labatto $LABATTO \
-add_incident_wave false 2>&1 |
grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt
echo "   "
echo "                       begin error calculation           "
echo "   "
#BEGIN error calculation
mpirun -np 2 $BIN_PATH/error_norm \
-my_file $BIN_PATH/fe_solution.h5m \
-my_is_partitioned $partition \
-norm_type $ERROR_TYPE \
-relative_error false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist  \
-ksp_monitor  \
-my_order 1 \
-my_max_post_proc_ref_level 0 \
-save_postproc_mesh true 2>&1 | grep --line-buffered -i " realtive error " | tee -a -i hard_sphere_p_${WAVE_NUMBER}_labatto_${LABATTO}.txt
#RENAME files inot their order and wave number.
mbconvert norm_error.h5m norm_error.vtk |
mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ODISP}.vtk
done

#SAVE the results to different directory
mkdir ${DIR_NAME}_${ERROR_TYPE}_sphere_k${WAVE_NUMBER}_p
mv norm_error_k${WAVE_NUMBER}_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_sphere_k${WAVE_NUMBER}_p

done
# done
#foo="Hello"
#foo="$foo World"
#echo $foo
#> Hello World
