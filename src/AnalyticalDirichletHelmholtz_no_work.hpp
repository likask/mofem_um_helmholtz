/** \file AnalyticalDirichletHelmholtz.hpp

  Enforce Dirichlet boundary condition for given analytical function,

*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __ANALYTICALDIRICHLETHELMHOLTZBC_HPP__
#define __ANALYTICALDIRICHLETHELMHOLTZBC_HPP__

using namespace boost::numeric;
using namespace MoFEM;

/** \brief Analytical Dirichlet boundary conditions
  \ingroup user_modules
  */
struct AnalyticalDirichletHelmholtzBC : public AnalyticalDirichletBC
{

  AnalyticalDirichletHelmholtzBC(MoFEM::Interface& m_field);

  struct DirichletBC_RhsOnly : public AnalyticalDirichletBC::DirichletBC {

    DirichletBC_RhsOnly(
      MoFEM::Interface& m_field,const string &field,Mat A,Vec X,Vec F
    );

    DirichletBC_RhsOnly(
      MoFEM::Interface& m_field,const string &field
    );

    Range *tRis_ptr;

    PetscErrorCode preProcess();
    PetscErrorCode postProcess();

  };

  struct DirichletBC_LhsOnly : public AnalyticalDirichletBC::DirichletBC {

    DirichletBC_LhsOnly(
      MoFEM::Interface& m_field,const string &field,Mat A,Vec X,Vec F
    );

    DirichletBC_LhsOnly(
      MoFEM::Interface& m_field,const string &field
    );

    Range *tRis_ptr;

    PetscErrorCode postProcess();

  };




  PetscErrorCode sortBc(
    DirichletBC &bc,Range &tris
  );



};




#endif //__ANALYTICALDIRICHLETHELMHOLTZBC_HPP__
