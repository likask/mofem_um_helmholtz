


//  struct ParticleVelocity {
//
//
//    /// \brief  Volume element
//    struct MyVolumeFE: public VolumeElementForcesAndSourcesCore {
//      int addToRank; ///< default value 1, i.e. assumes that geometry is approx. by quadratic functions.
//
//      MyVolumeFE(MoFEM::Interface &m_field,int add_to_rank):
//      VolumeElementForcesAndSourcesCore(m_field),
//      addToRank(add_to_rank) {}
//
//      int getRule(int order) { return order+addToRank; };
//    };
//
//    MyVolumeFE fe;
//    MyVolumeFE& getLoopFe() { return fe; }
//
//
//    /// \brief Surface element
//    struct MySurfaceFE: public FaceElementForcesAndSourcesCore {
//      int addToRank; ///< default value 1, i.e. assumes that geometry is approx. by quadratic functions.
//      MySurfaceFE(MoFEM::Interface &m_field,int add_to_rank):
//      FaceElementForcesAndSourcesCore(m_field),
//      addToRank(add_to_rank) {}
//
//      int getRule(int order) { return order+addToRank; };
//    };
//
//    boost::ptr_map<string,ForcesAndSurcesCore> feRhs; // surface element for LHS
//    boost::ptr_map<string,ForcesAndSurcesCore> feLhs; // surface element for RHS
//
//
//    /** \brief element data
//    * \ingroup mofem_helmholtz_elem
//    */
//    struct BlockData {
//      double dEnsity;
//      double aNgularFrequency;
//      Range tEts; ///< contains elements in block set
//    };
//    map<int,BlockData> blockData;
//
//    struct GlobalParameters {
//      pair<double,PetscBool> dEnsity;
//      pair<double,PetscBool> aNgularFrequency;
//    };
//    GlobalParameters globalParameters;
//
//    PetscErrorCode getGlobalParametersFromLineCommandOptions() {
//      PetscErrorCode ierr;
//
//      PetscFunctionBegin;
//      ierr = PetscOptionsBegin(mField.get_comm(),NULL,"Particle Velocity problem options","none"); CHKERRQ(ierr);
//
//      globalParameters.dEnsity.first = 1;
//      ierr = PetscOptionsReal("-density","material density","",
//        globalParameters.dEnsity.first,
//        &globalParameters.dEnsity.first,&globalParameters.dEnsity.second); CHKERRQ(ierr);
//      if(!globalParameters.dEnsity.second) {
//        SETERRQ(PETSC_COMM_SELF,1,"density not given, set in line command -density to fix problem");
//      }
//
//      globalParameters.aNgularFrequency.first = 1;
//      ierr = PetscOptionsReal("-angular_frequency","angular frequency","",
//        globalParameters.aNgularFrequency.first,
//        &globalParameters.aNgularFrequency.first,NULL); CHKERRQ(ierr);
//
//      ierr = PetscOptionsEnd(); CHKERRQ(ierr);
//      PetscFunctionReturn(0);
//    }
//
//    MoFEM::Interface &mField;
//    int addToRank; ///< default value 1, i.e. assumes that geometry is approx. by quadratic functions.
//
//    ParticleVelocity(
//      MoFEM::Interface &m_field):
//      mField(m_field),
//      fe(mField),
//      addToRank(1) {}
//
//
//    /** \brief Common data used by volume and surface elements
//    * \ingroup mofem_helmholtz_elem
//    */
//    struct CommonData {
//
//      map<string,VectorDouble > fieldAtGaussPts;
//      map<string,MatrixDouble > gradfieldAtGaussPts;
//      MatrixDouble hoCoords;
//
//      map<EntityType, vector< VectorInt > > imIndices;
//
//    };
//    CommonData commonData;
//
//
//
//    struct OpGetImIndices: public ForcesAndSurcesCore::UserDataOperator  {
//
//      CommonData &commonData;
//      const string reFieldName,imFieldName;
//      bool takeIndicesFromElementRowIndices;
//
//      OpGetImIndices(
//        const string re_field_name,const string im_field_name,CommonData &common_data
//      ):
//      ForcesAndSurcesCore::UserDataOperator(re_field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
//      commonData(common_data),
//      reFieldName(re_field_name),
//      imFieldName(im_field_name) {
//
//        if(reFieldName!=imFieldName) {
//
//          takeIndicesFromElementRowIndices = false;
//
//        } else {
//
//          takeIndicesFromElementRowIndices = true;
//
//        }
//
//      }
//
//      PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
//        PetscFunctionBegin;
//
//        int nb_row_dofs = data.getIndices().size();
//        if(nb_row_dofs==0) PetscFunctionReturn(0);
//
//        if(!side) {
//          commonData.imIndices[type].resize(6);
//        }
//
//        if(takeIndicesFromElementRowIndices) {
//
//          (commonData.imIndices[type])[side] = data.getIndices();
//
//        } else {
//
//          // Get rows and cols indices of imaginary part and assemble matrix.
//          // Note: However HELMHOLTZ_IMIM_FE element is not calculated, since
//          // matrix A on real elements is equal to matrix in imaginary elements,
//          // it need be to declared. Declaration indicate that on imaginary part,
//          // assembled matrix has non-zero values;
//          PetscErrorCode ierr;
//          ierr = getPorblemRowIndices(imFieldName,type,side,(commonData.imIndices[type])[side]); CHKERRQ(ierr);
//
//        }
//
//        PetscFunctionReturn(0);
//      }
//
//    };
//
//
//    /** \brief Calculate the value of the particle velocity at the
//         vertex in volume
//         FIX ME how to retrieve the field data and shape functions on vertex
//         instead of gaussian points.
//      */
//
//
//      struct OpGetValueAndGradAtGaussPts: public VolumeElementForcesAndSourcesCore::UserDataOperator {
//
//        CommonData &commonData;
//        const string fieldName;
//        OpGetValueAndGradAtGaussPts(const string field_name,CommonData &common_data):
//        VolumeElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
//        commonData(common_data),fieldName(field_name) {}
//
//        PetscErrorCode doWork(
//          int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
//          PetscFunctionBegin;
//          try {
//
//            int nb_dofs = data.getFieldData().size();
//            if(nb_dofs==0) PetscFunctionReturn(0);
//            int nb_gauss_pts = data.getN().size1();
//
//            VectorDouble &value = commonData.pressureAtGaussPts[fieldName];
//            MatrixDouble &gradient = commonData.gradPressureAtGaussPts[fieldName];
//
//            // initialize
//            value.resize(nb_gauss_pts,false);
//            gradient.resize(nb_gauss_pts,3,false);
//            if(type == MBVERTEX) {
//              gradient.clear();
//              value.clear();
//            }
//
//            for(int gg = 0;gg<nb_gauss_pts;gg++) {
//
//              value[gg] += inner_prod(data.getN(gg,nb_dofs),data.getFieldData());
//              //ublas::noalias(ublas::matrix_row<MatrixDouble >(gradient,gg)) +=
//              //prod( trans(data.getDiffN(gg,nb_dofs)), data.getFieldData() );
//              cblas_dgemv(CblasRowMajor,CblasTrans,
//                nb_dofs,3,1,
//                &data.getDiffN()(gg,0),3,
//                &data.getFieldData()[0],1,
//                1,&gradient(gg,0),1
//              );
//
//            }
//
//          } catch (const std::exception& ex) {
//            ostringstream ss;
//            ss << "throw in method: " << ex.what() << endl;
//            SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
//          }
//
//          PetscFunctionReturn(0);
//        }
//
//      };
