/** \file SecondOrderStress.hpp
 \ingroup mofem_helmholtz_elem

 \brief Operators and data structures for the calculation of
  second order flux term employed to retrieve the acoustic
  radiation force with Homogeneous material.

 */

/*
  This work is part of PhD thesis by on Micro-fluidics: Thomas Felix Xuan Meng
 */

/*
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
 * The header file should contains as least #include as possible for speed
 */


// #ifndef WITH_ADOL_C
//   #error "MoFEM need to be compiled with ADOL-C"
// #endif
/* \bug 1. Export the velocity data into vectors or .txt file,
   2. work for scalar; vector and tensor field. 3. Reynolds Stress */
struct SecondOrderStress: public VolumeElementForcesAndSourcesCore::UserDataOperator {

  MoFEM::Interface& mField;
  HelmholtzElement& helmholtzElement;
  moab::Interface &postProcMesh;
  vector<EntityHandle> &mapGaussPts;

  PostProcVolumeOnRefinedMesh::CommonData &commonData;
  string reFieldName;

  SecondOrderStress(
    MoFEM::Interface& m_field,
    HelmholtzElement& helmholtz_element,
    moab::Interface &post_proc_mesh,
    vector<EntityHandle> &map_gauss_pts,
    string re_field_name,
    PostProcVolumeOnRefinedMesh::CommonData &common_data):
    VolumeElementForcesAndSourcesCore::UserDataOperator(re_field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
    ///// FIX ME TAG NAME NEEDED. UserDataOperator can not accept both filed_name and tag_name.
    mField(m_field),
    helmholtzElement(helmholtz_element),
    postProcMesh(post_proc_mesh),mapGaussPts(map_gauss_pts),
    reFieldName(re_field_name),
    commonData(common_data) {}

  // struct GlobalParameters {
  //   /* density of the background medium */
  //   pair<double,PetscBool> dEnsity;
  //   pair<double,PetscBool> timeStep;
  //   pair<double,PetscBool> fRequency;
  //   pair<double,PetscBool> vElocity;
  // };
  //
  // GlobalParameters globalParameters;
  //
  // PetscErrorCode getGlobalParametersFromLineCommandOptions() {
  //   
  //
  //   PetscFunctionBegin;
  //   ierr = PetscOptionsBegin(mField.get_comm(),NULL,"Particle Velocity problem options","none"); CHKERRQ(ierr);
  //
  //   globalParameters.dEnsity.first = 1.0;
  //   ierr = PetscOptionsReal("-density","material density","",
  //     globalParameters.dEnsity.first,
  //     &globalParameters.dEnsity.first,NULL); CHKERRQ(ierr);
  //
  //   globalParameters.fRequency.first = 0;
  //     ierr = PetscOptionsReal("-frequency","frequency of the fluid","",
  //     globalParameters.fRequency.first,
  //     &globalParameters.fRequency.first,&globalParameters.fRequency.second); CHKERRQ(ierr);
  //
  //   globalParameters.vElocity.first = 0;
  //     ierr = PetscOptionsReal("-velocity","velocity of the fluid","",
  //     globalParameters.vElocity.first,
  //     &globalParameters.vElocity.first,&globalParameters.vElocity.second); CHKERRQ(ierr);
  //
  //   // globalParameters.timeStep.first = 5.0;
  //   //   ierr = PetscOptionsReal("-time_step",
  //   //   "How many time steps during the interval of time of wave propagation","",
  //   //   globalParameters.timeStep.first,
  //   //   &globalParameters.timeStep.first,
  //   //   &globalParameters.timeStep.second); CHKERRQ(ierr);
  //
  //     ierr = PetscOptionsEnd(); CHKERRQ(ierr);
  //   PetscFunctionReturn(0);
  // }



  PetscErrorCode doWork(
    int side,
    EntityType type,
    DataForcesAndSurcesCore::EntData &data) {
    PetscFunctionBegin;

    if(type != MBVERTEX) PetscFunctionReturn(0);
    if(data.getFieldData().size()==0) PetscFunctionReturn(0);

    
    
    // int rank = dof_ptr->get_max_rank();
    /* scalar field in 3D has rank = 1, rank vector = 3, rank tensorial = 9 */


    ierr = helmholtzElement.getGlobalParametersFromLineCommandOptions(); CHKERRQ(ierr);
    double dEnsity = helmholtzElement.globalParameters.dEnsity.first;
    double vElocity = helmholtzElement.globalParameters.vElocity.first;
    double fRequency = helmholtzElement.globalParameters.fRequency.first;
    double t = helmholtzElement.globalParameters.timeStep;

    VectorDouble velocity;
    double pRessure;
    VectorDouble Stress;
    VectorDouble AverageVelocity;
    // double AveragePressure;
    const complex< double > i( 0.0, 1.0 );
    double const1 = 1/(2*dEnsity*vElocity*vElocity);
    double const2 = 0.5 * dEnsity;

      Tag th_stress;
      int tag_length = 3;
      double def_VAL[tag_length];
      bzero(def_VAL,tag_length*sizeof(double));

      const char* name;
      name = (reFieldName+"STRESS").c_str();

      rval = postProcMesh.tag_get_handle(
        name,1,MB_TYPE_DOUBLE,th_stress,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);

      int nb_gauss_pts = data.getN().size1();
      if(mapGaussPts.size()!=(unsigned int)nb_gauss_pts) {
        SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconsistency");
      }

      for(int gg = 0;gg<nb_gauss_pts;gg++) {

        velocity.resize(3);
        AverageVelocity.resize(1);
        Stress.resize(1);

        pRessure = commonData.fieldMap[rowFieldName][gg][0];
        velocity[0] = (commonData.gradMap[rowFieldName][gg])(0,0)*(commonData.gradMap[rowFieldName][gg])(0,0);
        velocity[1] = (commonData.gradMap[rowFieldName][gg])(0,1)*(commonData.gradMap[rowFieldName][gg])(0,1);
        velocity[2] = (commonData.gradMap[rowFieldName][gg])(0,2)*(commonData.gradMap[rowFieldName][gg])(0,2);
        if(helmholtzElement.globalParameters.isMonochromaticWave.first) {
          Stress[0] = - const1 * pRessure * pRessure + const2 * (velocity[0] + velocity[1] + velocity[2]);


          Stress[0] *= std::real( exp(-i*2.0*M_PI*fRequency*t) / (-i*2.0*M_PI*fRequency) );
        } else {
          AverageVelocity[0] = (velocity[0]+velocity[1]+velocity[2]);
          Stress[0] = (- const1 * pRessure * pRessure + const2 * AverageVelocity[0]) * std::real(exp(i*2.0*M_PI*fRequency*t));
        }
        cerr << " \n Stress = \n " << Stress << endl;
        cerr << " \n time step ******** = \n " << t << endl;
        rval = postProcMesh.tag_set_data(th_stress,&mapGaussPts[gg],1,&Stress(0)); CHKERRQ_MOAB(rval);

      }

    PetscFunctionReturn(0);
  }

};
