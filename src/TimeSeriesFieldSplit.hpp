/* \file TimeSeriesFieldSplit.hpp
  \ingroup mofem_helmholtz_elem

*/

/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \brief Read impulse and apply FFT
  \ingroup mofem_helmholtz_elem

 First signal is read from text file. Currently is assumed that this signal
gives profile for plane wave. Spherical or other wave types can be easily
added and implemented.

 Before signal is applied the Discrete Fourier Transform (in space) is applied. Next
for each wavelength in space a phase shift is applied according to wave
speed. With that at hand incident wave and boundary conditions are applied
for each discrete point in time.

 Note that wave in space is superposition of incident waves. This
superposition makes wave with approximated wave profile which is read from
text file. Moreover obtained wave fulfill wave propagation equation a priori.

 Next Dictate Fourier Transform in time domain is applied to get frequency
(time) space. Having boundary conditions in frequency domain, Helmholtz
problem is solved for each wave length.

 At this point plane wave (or other wave) and scattering wave in frequency
space is known. The last and final step is to apply inverse Discrete Fourier
Transform to get acoustic wave pressures in discrete times.

Note that analysis is with assumption that all signals are periodic, as
result of essential property of Dictate Fourier Transform.

In following procedure a KISS FTP library <http://sourceforge.net/projects/kissfft/>
is used to do Fast Fourier Transform.

  */

struct PlaneIncidentWaveSacttrerData {

  Range tRis;

};

struct TimeSeriesFieldSplit {

  MoFEM::Interface& mField;
  HelmholtzElement& helmholtzElement;
  AnalyticalDirichletHelmholtzBC::DirichletBC& analyticalDitihletBcReal;
  AnalyticalDirichletHelmholtzBC::DirichletBC& analyticalDitihletBcImag;
  AnalyticalDirichletHelmholtzBC& analyticalBcReal;
  AnalyticalDirichletHelmholtzBC& analyticalBcImag;

  map<int,PlaneIncidentWaveSacttrerData>& planeWaveScatterData;
  bool dirichletBcSet;
  Range& bcDirichletTris;
  int readFile,debug;

  PostProcVolumeOnRefinedMesh postProc;

  TimeSeriesFieldSplit(MoFEM::Interface &m_field,
    HelmholtzElement& helmholtz_element,
    AnalyticalDirichletHelmholtzBC &analytical_bc_real,
    AnalyticalDirichletHelmholtzBC &analytical_bc_imag,
    AnalyticalDirichletHelmholtzBC::DirichletBC &analytical_ditihlet_bc_real,
    AnalyticalDirichletHelmholtzBC::DirichletBC &analytical_ditihlet_bc_imag,
    bool Dirichlet_bc_set,Range &bc_dirichlet_tris,
    map<int,PlaneIncidentWaveSacttrerData> &plane_wave_scatter_data):
      mField(m_field),helmholtzElement(helmholtz_element),
      analyticalBcReal(analytical_bc_real),
      analyticalBcImag(analytical_bc_imag),
      analyticalDitihletBcReal(analytical_ditihlet_bc_real),
      analyticalDitihletBcImag(analytical_ditihlet_bc_imag),
      dirichletBcSet(Dirichlet_bc_set),
      bcDirichletTris(bc_dirichlet_tris),
      planeWaveScatterData(plane_wave_scatter_data),
      readFile(0),
      debug(1),
      postProc(m_field)
      {}

  ErrorCode rval;
  PetscErrorCode ierr;

  /** \brief Read signal from text file
   */
  PetscErrorCode readData(const char* str,map<double,double> &series) {
    PetscFunctionBegin;

    char time_file_name[255];
    PetscBool flg = PETSC_TRUE;
    ierr = PetscOptionsGetString(PETSC_NULL,PETSC_NULL,str,time_file_name,255,&flg); CHKERRQ(ierr);
    if(flg != PETSC_TRUE) {
      SETERRQ1(PETSC_COMM_SELF,1,"*** ERROR %s (DATA FILE NEEDED)",str);
    }
    FILE *time_data = fopen(time_file_name,"r");
    if(time_data == NULL) {
      SETERRQ1(PETSC_COMM_SELF,1,"*** ERROR data file < %s > open unsucessfull",time_file_name);
    }
    double no1 = 0.0, no2 = 0.0;
    series[no1] = no2;
    while(! feof (time_data)){ //check if end-of-file is reached
      int n = fscanf(time_data,"%lf %lf",&no1,&no2);
      if((n <= 0)||((no1==0)&&(no2==0))) {
        fgetc(time_data);
        continue;
      }
      if(n != 2){
        SETERRQ1(PETSC_COMM_SELF,1,"*** ERROR read data file error (check input time data file) { n = %d }",n);
      }
      series[no1] = no2;
    }
    int r = fclose(time_data);

    if(debug) {
      map<double, double>::iterator tit = series.begin();
      for(;tit!=series.end();tit++) {
        PetscPrintf(PETSC_COMM_WORLD,"** read time series %3.2e time %3.2e\n",tit->first,tit->second);
      }
    }
    if(r!=0) {
      SETERRQ(PETSC_COMM_SELF,1,"*** ERROR file close unsuccessful");
    }
    readFile=1;

    PetscFunctionReturn(0);
  }

  map<double,double> sSeries;

  /** \brief This is wrapper to reading signal profile in space
   */
  PetscErrorCode readSpaceData() {
    PetscFunctionBegin;
    ierr = readData("-space_data",sSeries); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  /** \brief Read all text files

    At this point only signal profile in space is read.

  */
  PetscErrorCode readData() {
    PetscFunctionBegin;
    ierr = readSpaceData(); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  boost::shared_array<kiss_fft_cpx> complexIn;  ///< Impulse amplitudes in space (imaginary)
  boost::shared_array<kiss_fft_cpx> complexOut; ///< Impulse amplitude in space (real)

  kiss_fft_cfg forwardCfg;
  kiss_fft_cfg inverseCfg;

  /** \brief Apply Forward FFT for signal.
  */
  PetscErrorCode forwardSignalDft(
    map<double,double> series,
    boost::shared_array<kiss_fft_cpx> &complex_in,
    boost::shared_array<kiss_fft_cpx> &complex_out
  ) {
    PetscFunctionBegin;

    int n = sSeries.size();
    if(!helmholtzElement.globalParameters.nbOfPointsInTime.second) {
      helmholtzElement.globalParameters.nbOfPointsInTime.first = n;
    }

    complex_in = boost::shared_array<kiss_fft_cpx>(new kiss_fft_cpx[n]);
    map<double,double>::iterator mit = sSeries.begin();
    for(int ii = 0;mit!=sSeries.end();mit++,ii++) {
      complex_in[ii].r = mit->second;
      complex_in[ii].i = 0;
    }

    complex_out = boost::shared_array<kiss_fft_cpx>(new kiss_fft_cpx[n]);
    /* forwardCfg returned from kiss_fft_alloc */
    kiss_fft(forwardCfg,complex_in.get(),complex_out.get());

    /*const complex< double > i( 0.0, 1.0 );
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    for(int f = 0;f<n;f++) {
      double wave_number = 2*M_PI*f/(double)n;
      complex<double> p = 0;
      for(int k = 0;k<n;k++) {
        p += (complex_out[k].r+i*complex_out[k].i)*exp(i*(wave_number*k) - i*(2*M_PI*k*0.21) );
      }
      p /= n;
      if(!rank) {
        cerr << std::real(p) << " " << std::imag(p) << endl;
      }
    }*/

    PetscFunctionReturn(0);
  }

  /** \brief Save signal data from text file
   */
  PetscErrorCode saveData(map<double,double> &series) {


    PetscFunctionBegin;
    string filename("frequency_signal.txt" );
    ofstream ofs( filename.c_str() );
    ofs.precision( 18 );
    cout.precision( 18 );
    for(int n = 0;n!=sSeries.size();n++) {
      ofs << n << "\t" << helmholtzElement.globalParameters.complexOut[n].r << "\t" << helmholtzElement.globalParameters.complexOut[n].i << "\t" << endl;
    }
    PetscPrintf(PETSC_COMM_WORLD,"\n\ntime series in frequency domain saved for frequency range %d\n",sSeries.size());

    PetscFunctionReturn(0);
  }

  /** \brief This is wrapper to saving signal profile in frequency
   */
  PetscErrorCode saveFrequencyData() {
    PetscFunctionBegin;
    ierr = saveData(sSeries); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  /** \brief Apply Inverse FFT for incident wave
   */
  PetscErrorCode forwardDftIncidentWave() {
    PetscFunctionBegin;

    PetscBool is_partitioned = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-my_is_partitioned",&is_partitioned,PETSC_NULL); CHKERRQ(ierr);

    KSP approx_incindent_wave_solver;
    Mat A_approx_incident_wave;
    Vec D_approx_incident_wave;
    vector<Vec> F(2);
    vector<VecScatter> scatter_incident_wave(2);

    ierr = mField.MatCreateMPIAIJWithArrays("INCIDENT_WAVE",&A_approx_incident_wave); CHKERRQ(ierr);
    ierr = KSPCreate(PETSC_COMM_WORLD,&approx_incindent_wave_solver); CHKERRQ(ierr);
    ierr = KSPSetOperators(approx_incindent_wave_solver,A_approx_incident_wave,A_approx_incident_wave); CHKERRQ(ierr);
    ierr = KSPSetFromOptions(approx_incindent_wave_solver); CHKERRQ(ierr);
    ierr = mField.VecCreateGhost("INCIDENT_WAVE",ROW,&F[0]); CHKERRQ(ierr);
    ierr = VecDuplicate(F[0],&F[1]); CHKERRQ(ierr);
    ierr = mField.VecCreateGhost("INCIDENT_WAVE",ROW,&D_approx_incident_wave); CHKERRQ(ierr);
    ierr = mField.VecScatterCreate(
      D_approx_incident_wave,"INCIDENT_WAVE","rePRES",ROW,pSeriesIncidentWave[0][0],"PRESSURE_IN_TIME","P",ROW,&scatter_incident_wave[0]
    ); CHKERRQ(ierr);
    ierr = mField.VecScatterCreate(
      D_approx_incident_wave,"INCIDENT_WAVE","rePRES",ROW,pSeriesIncidentWave[1][0],"PRESSURE_IN_TIME","P",ROW,&scatter_incident_wave[1]
    ); CHKERRQ(ierr);

    if(!helmholtzElement.globalParameters.signalLength.second) {
      SETERRQ(PETSC_COMM_SELF,1,"Signal length not set, -signal_length");
    }
    if(!helmholtzElement.globalParameters.signalDuration.second) {
      SETERRQ(PETSC_COMM_SELF,1,"Signal duration not set, -signal_duration");
    }

    ierr = MatZeroEntries(A_approx_incident_wave); CHKERRQ(ierr);
    int n = sSeries.size();
    int m = helmholtzElement.globalParameters.nbOfPointsInTime.first;
    // FIXME not elegant part of code by Thomas
    IncidentWaveDFT function_evaluator2;
    IncidentWavePointSourceDFT function_evaluator1;

    double signalLength = 0;
    if(helmholtzElement.globalParameters.vElocity.second) {
      signalLength = helmholtzElement.globalParameters.signalLength.first*(helmholtzElement.globalParameters.vElocity.first/4000);
    } else {
      signalLength = helmholtzElement.globalParameters.signalLength.first;
    }

    if(helmholtzElement.globalParameters.sourceCoordinate.second) {
      function_evaluator1 = IncidentWavePointSourceDFT(
        signalLength,
        helmholtzElement.globalParameters.signalDuration.first,
        helmholtzElement.globalParameters.sourceCoordinate.first,
        helmholtzElement.globalParameters.aTtenuation.first,
        helmholtzElement.globalParameters.rAdius.first,
        helmholtzElement.globalParameters.complexWaveNumber.first,
        helmholtzElement.globalParameters.fRequency.first,
        helmholtzElement.globalParameters.vElocity.first,
        helmholtzElement.globalParameters.transmissionCoefficient.first,
        helmholtzElement.globalParameters.dEnsity.first,
        complexOut,n,m,0,
        false,helmholtzElement.globalParameters.isRayleigh.first
      );
      ierr = calculate_matrix_and_vector(
        mField,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","rePRES",A_approx_incident_wave,F,function_evaluator1
      ); CHKERRQ(ierr);
    } else {
      function_evaluator2 = IncidentWaveDFT(
          signalLength,
          helmholtzElement.globalParameters.signalDuration.first,
          helmholtzElement.globalParameters.waveOscilationDirection.first,
          helmholtzElement.globalParameters.aTtenuation.first,
          helmholtzElement.globalParameters.rAdius.first,
          helmholtzElement.globalParameters.complexWaveNumber.first,
          helmholtzElement.globalParameters.fRequency.first,
          helmholtzElement.globalParameters.vElocity.first,
          helmholtzElement.globalParameters.transmissionCoefficient.first,
          helmholtzElement.globalParameters.dEnsity.first,
          complexOut,n,m,0,
          false,helmholtzElement.globalParameters.isRayleigh.first
        );
      ierr = calculate_matrix_and_vector(
        mField,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","rePRES",A_approx_incident_wave,F,function_evaluator2
      ); CHKERRQ(ierr);
    }

    // ierr = MatZeroEntries(A_approx_incident_wave); CHKERRQ(ierr);
    // if(helmholtzElement.globalParameters.sourceCoordinate.second) {
    //   ierr = calculate_matrix_and_vector(
    //     mField,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","imPRES",A_approx_incident_wave,F,function_evaluator1
    //   ); CHKERRQ(ierr);
    // } else {
    //   ierr = calculate_matrix_and_vector(
    //     mField,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","imPRES",A_approx_incident_wave,F,function_evaluator2
    //   ); CHKERRQ(ierr);
    // }
    // ierr = MatView(A_approx_incident_wave,PETSC_VIEWER_DRAW_WORLD);
    // std::string wait;
    // std::cin >> wait;
    // double norm_A;
    // ierr = MatNorm(A_approx_incident_wave,NORM_FROBENIUS,&norm_A);
    // cerr <<"\n matrix A norm =  \n" << norm_A << endl;

    for(int ss = 0;ss<2;ss++) {
      ierr = VecGhostUpdateBegin(F[ss],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F[ss],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    }

    ierr = KSPSetUp(approx_incindent_wave_solver); CHKERRQ(ierr);

    // For this it is real time and space is in frequencies.
    // It is assumed that wave is periodic.
    for(int t = 0;t<m;t++) {

      PetscPrintf(PETSC_COMM_WORLD,"\n\nTime step %d, out of %d\n",t,n);

      for(int ss = 0;ss<2;ss++) {
        ierr = VecZeroEntries(F[ss]); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(F[ss],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(F[ss],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      }

      if(helmholtzElement.globalParameters.sourceCoordinate.second) {
        function_evaluator1.timeStep = t;
        // Calculate vector it only once
        ierr = calculate_matrix_and_vector(
          mField,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","rePRES",PETSC_NULL,F,function_evaluator1
        ); CHKERRQ(ierr);
      } else {
        function_evaluator2.timeStep = t;
        ierr = calculate_matrix_and_vector(
          mField,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","rePRES",PETSC_NULL,F,function_evaluator2
        ); CHKERRQ(ierr);
      }


      // Solve incident wave approximation problem
      for(int ss = 0;ss<2;ss++) {

        /* the right hand size vector required to be normalized in case
          the acoustic problem is solved in micro-scale, the residual is extremely
          small that could not be detected */
        double nrm2_F;
        ierr = VecNorm(F[ss],NORM_2,&nrm2_F);  CHKERRQ(ierr);
        ierr = ierr = VecScale(F[ss],1/nrm2_F); CHKERRQ(ierr);

        ierr = VecZeroEntries(D_approx_incident_wave); CHKERRQ(ierr);
        ierr = KSPSolve(approx_incindent_wave_solver,F[ss],D_approx_incident_wave); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(D_approx_incident_wave,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(D_approx_incident_wave,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        /* scale back to normal */
        ierr = ierr = VecScale(D_approx_incident_wave,nrm2_F); CHKERRQ(ierr);


        // double nrm2_T;
        // // ierr = VecNorm(F[1],NORM_2,&nrm2_F);  CHKERRQ(ierr);
        // cerr << "\n nomr of vector F[1]: \n" << " = "<< nrm2_F << endl;
        // // ierr = VecView(F[1],PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
        // ierr = VecNorm(D_approx_incident_wave,NORM_2,&nrm2_T);  CHKERRQ(ierr);
        // cerr << "\n nomr of vector D_approx_incident_wave: \n" << nrm2_T << endl;
        // std::string wait;
        // std::cin >> wait;

        ierr = VecScatterBegin(
          scatter_incident_wave[ss],D_approx_incident_wave,pSeriesIncidentWave[ss](t),INSERT_VALUES,SCATTER_FORWARD
        ); CHKERRQ(ierr);
        ierr = VecScatterEnd(
          scatter_incident_wave[ss],D_approx_incident_wave,pSeriesIncidentWave[ss](t),INSERT_VALUES,SCATTER_FORWARD
        ); CHKERRQ(ierr);
      }

    }

    ierr = KSPDestroy(&approx_incindent_wave_solver); CHKERRQ(ierr);
    ierr = MatDestroy(&A_approx_incident_wave); CHKERRQ(ierr);
    ierr = VecDestroy(&F[0]); CHKERRQ(ierr);
    ierr = VecDestroy(&F[1]); CHKERRQ(ierr);
    ierr = VecDestroy(&D_approx_incident_wave); CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scatter_incident_wave[0]); CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scatter_incident_wave[1]); CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  /** \brief Apply Forward FFT and compose wave

   */
  PetscErrorCode forwardSpaceDft() {
    PetscFunctionBegin;
    int n = sSeries.size();
    helmholtzElement.globalParameters.complexOutSize = n;
    forwardCfg = kiss_fft_alloc(n, 0, NULL, NULL);

    // Transform impulse in physical space to frequency space domain
    ierr = forwardSignalDft(sSeries,complexIn,complexOut); CHKERRQ(ierr);
    helmholtzElement.globalParameters.complexOut = complexOut;

    // Add incident wave
    PetscBool add_incident_wave = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-add_incident_wave",&add_incident_wave,NULL); CHKERRQ(ierr);
    if(add_incident_wave) {
      ierr = forwardDftIncidentWave(); CHKERRQ(ierr);
    }

    PetscFunctionReturn(0)  ;
  }

  vector<ublas::vector<Vec> > pSeriesIncidentWave;
  vector<ublas::vector<Vec> > pSeriesScatterWave;
  vector<ublas::vector<Vec> > pSeriesRadiationForce;
  vector<VecScatter> scatterPressure;


  PetscErrorCode createSeries(Vec T,vector<ublas::vector<Vec> > &p_series) {
    PetscFunctionBegin;

    int n = sSeries.size();
    if(helmholtzElement.globalParameters.nbOfPointsInTime.second) {
      n = helmholtzElement.globalParameters.nbOfPointsInTime.first;
    }

    p_series.resize(2);
    p_series[0].resize(n);
    p_series[1].resize(n);

    for(int ss = 0;ss<2;ss++) {
      ierr = mField.VecCreateGhost("PRESSURE_IN_TIME",ROW,&p_series[ss][0]); CHKERRQ(ierr);
      for(int k = 1;k<n;k++) {
        //Duplicate the ghost format to all the vectors inside real and imaginary containers.
        ierr = VecDuplicate(p_series[ss][0],&p_series[ss][k]); CHKERRQ(ierr);
      }
    }


    PetscFunctionReturn(0);
  }

  PetscErrorCode createIncidentSeries(Vec T,vector<ublas::vector<Vec> > &p_series) {
    PetscFunctionBegin;

    int n = sSeries.size();

    p_series.resize(2);
    p_series[0].resize(n);
    p_series[1].resize(n);

    for(int ss = 0;ss<2;ss++) {
      ierr = mField.VecCreateGhost("PRESSURE_IN_TIME",ROW,&p_series[ss][0]); CHKERRQ(ierr);
      for(int k = 1;k<n;k++) {
        //Duplicate the ghost format to all the vectors inside real and imaginary containers.
        ierr = VecDuplicate(p_series[ss][0],&p_series[ss][k]); CHKERRQ(ierr);
      }
    }


    PetscFunctionReturn(0);
  }

  /** \brief Destroy pressure series.
  */
  PetscErrorCode destroySeries(vector<ublas::vector<Vec> > &p_series) {
    PetscFunctionBegin;

    int n = sSeries.size();
    if(helmholtzElement.globalParameters.nbOfPointsInTime.second) {
      n = helmholtzElement.globalParameters.nbOfPointsInTime.first;
    }

    for(int ss = 0;ss<2;ss++) {
      for(int k = 0;n<k;k++) {
        ierr = VecDestroy(&p_series[ss][k]); CHKERRQ(ierr);
      }
    }

    PetscFunctionReturn(0);
  }

  /** \brief Create vectors for each wave lengths

 There are two series, for real and imaginary values. Each series keeps
pressures at time steps or wave length depending on stage of algorithm.

  */
  PetscErrorCode createPressureSeries(Vec T) {
    PetscFunctionBegin;

    PetscBool add_incident_wave = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-add_incident_wave",&add_incident_wave,NULL); CHKERRQ(ierr);
    if(add_incident_wave) {
      ierr = createSeries(T,pSeriesIncidentWave); CHKERRQ(ierr);
    }

    ierr = createSeries(T,pSeriesScatterWave); CHKERRQ(ierr);

    scatterPressure.resize(2);
    ierr = mField.VecScatterCreate(
      T,"ACOUSTIC_PROBLEM","rePRES",ROW,pSeriesScatterWave[0][0],"PRESSURE_IN_TIME","P",ROW,&scatterPressure[0]
    ); CHKERRQ(ierr);
    ierr = mField.VecScatterCreate(
      T,"ACOUSTIC_PROBLEM","imPRES",ROW,pSeriesScatterWave[0][0],"PRESSURE_IN_TIME","P",ROW,&scatterPressure[1]
    ); CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  /** \brief Destroy pressure series.
  */
  PetscErrorCode destroyPressureSeries() {
    PetscFunctionBegin;


    PetscBool add_incident_wave = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-add_incident_wave",&add_incident_wave,NULL); CHKERRQ(ierr);
    if(add_incident_wave) {
      ierr = destroySeries(pSeriesIncidentWave); CHKERRQ(ierr);
    }

    ierr = destroySeries(pSeriesScatterWave); CHKERRQ(ierr);

    for(int ss = 0;ss<2;ss++) {
      ierr = VecScatterDestroy(&scatterPressure[ss]); CHKERRQ(ierr);
    }

    kiss_fft_cleanup();

    PetscFunctionReturn(0);
  }

  // PetscErrorCode solveDirichletBcs(  ) {
  //   PetscFunctionBegin;
  //
  //
  // }

  /** \brief Solve problem for each wave number.

  Calculate right hand vector for boundary condition at each time step. Each
  boundary condition is calculated as superposition of plane waves, having given
  Fourier Transform of signal and phase shift in space according to wave speed in medium.

  Next Forward FFT is applied to right hand vector and for each wave length
  Helmholtz problem is solved. This is most time consuming stage of algorithm.

  Currently only boundary conditions are implemented for hard and mix surface. Other types
  of boundary conditions could be easily implemented.

  */
  PetscErrorCode solveForwardDFT(Mat A,Vec F,Vec T) {
    PetscFunctionBegin;

    int n = sSeries.size();
    int m = helmholtzElement.globalParameters.nbOfPointsInTime.first;
    forwardCfg = kiss_fft_alloc(m, 0, NULL, NULL);
    // helmholtzElement.globalParameters.complexOutSize = n;

    // Zero vectors
    ierr = VecZeroEntries(T); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

    vector<ublas::vector<Vec> > rhs_series;
    ierr = createSeries(T,rhs_series); CHKERRQ(ierr);

    double signal_length = helmholtzElement.globalParameters.signalLength.first;
    double signal_duration = helmholtzElement.globalParameters.signalDuration.first;
    double speed = signal_length/signal_duration;

    /* define Dirichlet Bc operators */
    AnalyticalDirichletHelmholtzBC::DirichletBC_RhsOnly analytical_ditihlet_bc_rhs_real(mField,"rePRES",PETSC_NULL,T,F);
    AnalyticalDirichletHelmholtzBC::DirichletBC_RhsOnly analytical_ditihlet_bc_rhs_imag(mField,"imPRES",PETSC_NULL,T,F);
    AnalyticalDirichletHelmholtzBC::DirichletBC_LhsOnly analytical_ditihlet_bc_lhs_real(mField,"rePRES",A,PETSC_NULL,PETSC_NULL);
    AnalyticalDirichletHelmholtzBC::DirichletBC_LhsOnly analytical_ditihlet_bc_lhs_imag(mField,"imPRES",A,PETSC_NULL,PETSC_NULL);
    // FIXME not elegant part of code by Thomas
    boost::shared_ptr<IncidentWavePointSourceDFT> function_evaluator1;
    boost::shared_ptr<IncidentWaveDFT> function_evaluator2;
    if(dirichletBcSet) {

      if(!helmholtzElement.globalParameters.sourceCoordinate.second) {
        // note negative field, scatter field should cancel incident wave
        function_evaluator2 = boost::shared_ptr<IncidentWaveDFT>(
          new IncidentWaveDFT(
          signal_length,
          signal_duration,
          helmholtzElement.globalParameters.waveOscilationDirection.first,
          helmholtzElement.globalParameters.aTtenuation.first,
          helmholtzElement.globalParameters.rAdius.first,
          helmholtzElement.globalParameters.complexWaveNumber.first,
          helmholtzElement.globalParameters.fRequency.first,
          helmholtzElement.globalParameters.vElocity.first,
          helmholtzElement.globalParameters.transmissionCoefficient.first,
          helmholtzElement.globalParameters.dEnsity.first,
          helmholtzElement.globalParameters.complexOut,n,m,0,
          helmholtzElement.globalParameters.isRadiation.first,
          helmholtzElement.globalParameters.isRayleigh.first)
        );
      } else {
        // note negative field, scatter field should cancel incident wave
        function_evaluator1 = boost::shared_ptr<IncidentWavePointSourceDFT>(
          new IncidentWavePointSourceDFT(
          signal_length,
          signal_duration,
          helmholtzElement.globalParameters.sourceCoordinate.first,
          helmholtzElement.globalParameters.aTtenuation.first,
          helmholtzElement.globalParameters.rAdius.first,
          helmholtzElement.globalParameters.complexWaveNumber.first,
          helmholtzElement.globalParameters.fRequency.first,
          helmholtzElement.globalParameters.vElocity.first,
          helmholtzElement.globalParameters.transmissionCoefficient.first,
          helmholtzElement.globalParameters.dEnsity.first,
          helmholtzElement.globalParameters.complexOut,n,m,0,
          helmholtzElement.globalParameters.isRadiation.first,
          helmholtzElement.globalParameters.isRayleigh.first)
        );
      }

    }


    // Calculate boundary conditions at time steps
    for(int t = 0;t<m;t++) {

      double wave_number = 2*M_PI*t/signal_length;

      PetscPrintf(PETSC_COMM_WORLD,"Calculate rhs for frequency %d for wave number (space) %3.4g out of %d\n",t,wave_number,m);

      helmholtzElement.globalParameters.waveNumber.first = wave_number;
      helmholtzElement.globalParameters.timeStep = t;
      /* set the Dirichlet BCs */
      if(dirichletBcSet) {

        {
          map<int,PlaneIncidentWaveSacttrerData>::iterator mit = planeWaveScatterData.begin();
          for(;mit!= planeWaveScatterData.end();mit++) {

            analyticalBcReal.approxField.getLoopFeApprox().getOpPtrVector().clear();
            analyticalBcImag.approxField.getLoopFeApprox().getOpPtrVector().clear();

            if(!helmholtzElement.globalParameters.sourceCoordinate.second) {
              function_evaluator2->timeStep = t;
              ierr = analyticalBcReal.setApproxOps(
                mField,"rePRES",mit->second.tRis,function_evaluator2,GenericAnalyticalSolution::REAL
              ); CHKERRQ(ierr);
              ierr = analyticalBcImag.setApproxOps(
                mField,"imPRES",mit->second.tRis,function_evaluator2,GenericAnalyticalSolution::IMAG
              ); CHKERRQ(ierr);
            } else {
              function_evaluator1->timeStep = t;
              ierr = analyticalBcReal.setApproxOps(
                mField,"rePRES",mit->second.tRis,function_evaluator1,GenericAnalyticalSolution::REAL
              ); CHKERRQ(ierr);
              ierr = analyticalBcImag.setApproxOps(
                mField,"imPRES",mit->second.tRis,function_evaluator1,GenericAnalyticalSolution::IMAG
              ); CHKERRQ(ierr);
            }

          }

        }
        // Solve for analytical Dirichlet bc dofs
        ierr = analyticalBcReal.setProblem(mField,"BCREAL_PROBLEM"); CHKERRQ(ierr);
        ierr = analyticalBcImag.setProblem(mField,"BCIMAG_PROBLEM"); CHKERRQ(ierr);
        ierr = analyticalBcReal.solveProblem(
          mField,"BCREAL_PROBLEM","BCREAL_FE",analyticalDitihletBcReal,bcDirichletTris
        ); CHKERRQ(ierr);
        ierr = analyticalBcImag.solveProblem(
          mField,"BCIMAG_PROBLEM","BCIMAG_FE",analyticalDitihletBcImag,bcDirichletTris
        ); CHKERRQ(ierr);

        ierr = analyticalBcReal.destroyProblem(); CHKERRQ(ierr);
        ierr = analyticalBcImag.destroyProblem(); CHKERRQ(ierr);

        ierr = analyticalBcReal.sortBc(analytical_ditihlet_bc_rhs_real,bcDirichletTris); CHKERRQ(ierr);
        ierr = analyticalBcImag.sortBc(analytical_ditihlet_bc_rhs_imag,bcDirichletTris); CHKERRQ(ierr);
        ierr = analyticalBcReal.sortBc(analytical_ditihlet_bc_lhs_real,bcDirichletTris); CHKERRQ(ierr);
        ierr = analyticalBcImag.sortBc(analytical_ditihlet_bc_lhs_imag,bcDirichletTris); CHKERRQ(ierr);

      }
      /* end */

      ierr = VecZeroEntries(F); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

      if(dirichletBcSet) {
        ierr = mField.problem_basic_method_preProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_rhs_real); CHKERRQ(ierr);
        ierr = mField.problem_basic_method_preProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_rhs_imag); CHKERRQ(ierr);
      } // THIS MAY DID NOT SET THE VALUE CORRECTLY.
      // double nrm2_F;
      // ierr = VecNorm(F,NORM_2,&nrm2_F); CHKERRQ(ierr);
      // PetscPrintf(PETSC_COMM_WORLD," \n \n \n nrm2_F before = %6.4e\n",nrm2_F);

      ierr = helmholtzElement.calculateF("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);
      ierr = VecAssemblyBegin(F); CHKERRQ(ierr);
      ierr = VecAssemblyEnd(F); CHKERRQ(ierr);
      ierr = VecScale(F,-1); CHKERRQ(ierr);

      // ierr = VecView(F,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

      if(dirichletBcSet) {
        ierr = mField.problem_basic_method_postProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_rhs_real); CHKERRQ(ierr);
        ierr = mField.problem_basic_method_postProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_rhs_imag); CHKERRQ(ierr);
      }
      ierr = VecAssemblyBegin(F); CHKERRQ(ierr);
      ierr = VecAssemblyEnd(F); CHKERRQ(ierr);

      for(int ss = 0;ss<2;ss++) {
        ierr = VecScatterBegin(
          scatterPressure[ss],F,rhs_series[ss](t),INSERT_VALUES,SCATTER_FORWARD
        ); CHKERRQ(ierr);
        ierr = VecScatterEnd(
          scatterPressure[ss],F,rhs_series[ss](t),INSERT_VALUES,SCATTER_FORWARD
        ); CHKERRQ(ierr);
      }

    }
    //get the velocity of the material for cavity
    pair<double,PetscBool> vElocity;
    vElocity.first = 0;
    ierr = PetscOptionsBegin(mField.get_comm(),NULL,"Helmholtz problem options","none"); CHKERRQ(ierr);
    ierr = PetscOptionsReal("-velocity","cavity velocity","",
      vElocity.first,
      &vElocity.first,&vElocity.second); CHKERRQ(ierr);
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    // From now transform right hand vector to frequency domain
    ierr = seriesForwardDft(rhs_series,true); CHKERRQ(ierr);

    // field split
    ublas::vector<IS> nested_is_rows(2);
    ublas::matrix<Mat> nested_matrices(2,2);
    for(int i = 0;i!=2;i++) {
      nested_is_rows[i] = PETSC_NULL;
    }

    // get ROW and COL indices
    ierr = mField.ISCreateProblemFieldAndRank(
      "ACOUSTIC_PROBLEM",
      ROW,"rePRES",0,1,
      &nested_is_rows[0]
    ); CHKERRQ(ierr);
    ierr = mField.ISCreateProblemFieldAndRank(
      "ACOUSTIC_PROBLEM",
      ROW,"imPRES",0,1,
      &nested_is_rows[1]
    ); CHKERRQ(ierr);

    Mat SubA,SubC,SubConjC;
    // ierr = MatGetSubMatrix(A,nested_is_rows[0],nested_is_rows[0],MAT_INITIAL_MATRIX,&SubA); CHKERRQ(ierr);
    // ierr = MatGetSubMatrix(A,nested_is_rows[1],nested_is_rows[0],MAT_INITIAL_MATRIX,&SubC); CHKERRQ(ierr);
    // ierr = MatGetSubMatrix(A,nested_is_rows[0],nested_is_rows[1],MAT_INITIAL_MATRIX,&SubConjC); CHKERRQ(ierr);
    // // field split


    // Solve problem in frequency domain
    for(int f = 0;f<m;f++) {

      const complex< double > i( 0.0, 1.0 );
      double wave_number = (2.0*M_PI*f/speed);
      /* velocity of different material */
      if(vElocity.second) {
        wave_number = ((2.0*M_PI*f)/(vElocity.first));
        // wave_number += i*helmholtzElement.globalParameters.complexWaveNumber.first;
      }

      PetscPrintf(PETSC_COMM_WORLD,"\n\nSolve Helmholtz problem for frequency %d for wave number (time) %3.4g out of %d\n",f,wave_number,m);

      map<int,HelmholtzElement::VolumeData>::iterator vit = helmholtzElement.volumeData.begin();
      for(;vit != helmholtzElement.volumeData.end();vit++) {
        vit->second.waveNumber = wave_number;
      }

      /* matrix evaluation time (integration and assembling) */
      PetscLogDouble t1,t2;
      PetscLogDouble v1,v2;
      ierr = PetscTime(&v1); CHKERRQ(ierr);
      ierr = PetscGetCPUTime(&t1); CHKERRQ(ierr);

      map<int,HelmholtzElement::SurfaceData>::iterator sit =  helmholtzElement.surfaceHomogeneousBcData.begin();
      // if(vElocity.second) {
      //   for(;sit != helmholtzElement.surfaceHomogeneousBcData.end(); sit++) {
      //     sit->second.aDmittance_imag = -aDmittanceImag;
      //   }
      // }

      sit =  helmholtzElement.sommerfeldBcData.begin();
      for(;sit != helmholtzElement.sommerfeldBcData.end(); sit++) {
        // cerr << "\n sommerfeldBcData \n " << endl;
        sit->second.aDmittance_imag = -(wave_number);
      }
      sit =  helmholtzElement.baylissTurkelBcData.begin();
      for(;sit != helmholtzElement.baylissTurkelBcData.end(); sit++) {
        sit->second.aDmittance_imag = -(wave_number);
      }

      // Zero matrix
      ierr = MatZeroEntries(A); CHKERRQ(ierr);
      // Calculate matrix
      ierr = helmholtzElement.calculateA("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);
      if(dirichletBcSet) {
        ierr = mField.problem_basic_method_postProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_lhs_real); CHKERRQ(ierr);
        ierr = mField.problem_basic_method_postProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_lhs_imag); CHKERRQ(ierr);
      }
      ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

      // field split
      for(int i = 0;i!=2;i++) {
        for(int j = 0;j!=2;j++) {
          nested_matrices(i,j) = PETSC_NULL;
        }
      }

      // Mat SubA,SubC,SubConjC;
      ierr = MatGetSubMatrix(A,nested_is_rows[0],nested_is_rows[0],MAT_INITIAL_MATRIX,&SubA); CHKERRQ(ierr);
      ierr = MatGetSubMatrix(A,nested_is_rows[1],nested_is_rows[0],MAT_INITIAL_MATRIX,&SubC); CHKERRQ(ierr);
      ierr = MatGetSubMatrix(A,nested_is_rows[0],nested_is_rows[1],MAT_INITIAL_MATRIX,&SubConjC); CHKERRQ(ierr);

      // ierr = MatGetSubMatrix(A,nested_is_rows[0],nested_is_rows[0],MAT_REUSE_MATRIX,&SubA); CHKERRQ(ierr);
      // ierr = MatGetSubMatrix(A,nested_is_rows[1],nested_is_rows[0],MAT_REUSE_MATRIX,&SubC); CHKERRQ(ierr);
      // ierr = MatGetSubMatrix(A,nested_is_rows[0],nested_is_rows[1],MAT_REUSE_MATRIX,&SubConjC); CHKERRQ(ierr);

      ierr = MatAssemblyBegin(SubA,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(SubA,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyBegin(SubC,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(SubC,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyBegin(SubConjC,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(SubConjC,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

      // if(debug) {
      // PetscInt nestM,nestN,nestLocalIS1,nestGlobalIS1,nestLocalIS2,nestGlobalIS2;
      // // ierr = MatNestGetISs(B,&nested_is_rows[0],NULL); CHKERRQ(ierr);
      // ierr = ISGetSize(nested_is_rows[0],&nestGlobalIS1); CHKERRQ(ierr);
      // ierr = ISGetLocalSize(nested_is_rows[0],&nestLocalIS1); CHKERRQ(ierr);
      // ierr = ISGetSize(nested_is_rows[1],&nestGlobalIS2); CHKERRQ(ierr);
      // ierr = ISGetLocalSize(nested_is_rows[1],&nestLocalIS2); CHKERRQ(ierr);
      // cerr << "\n nest matrix LocalIS1 = \n " << nestLocalIS1 << "\n nest matrix LocalIS2 = \n " << nestLocalIS2 << endl;
      // cerr << "\n nest matrix GlobalIS1 = \n " << nestGlobalIS1 << "\n nest matrix GlobalIS2 = \n " << nestGlobalIS2 << endl;
      // std::string wait;
      // std::cin >> wait;
      // ierr = MatView(B,PETSC_VIEWER_DRAW_WORLD);
      // std::cin >> wait;
        // cerr << "A" << endl;
        // // ierr = MatView(A,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
        // ierr = MatView(A,PETSC_VIEWER_DRAW_WORLD); CHKERRQ(ierr);
        // std::string wait;
        // std::cin >> wait;
        // cerr << "Nested SubA" << endl;
        // // MatView(SubA,PETSC_VIEWER_STDOUT_WORLD);
        // ierr = MatView(SubA,PETSC_VIEWER_DRAW_WORLD); CHKERRQ(ierr);
        // std::cin >> wait;
        // cerr << "Nested SubC" << endl;
        // // MatView(SubC,PETSC_VIEWER_STDOUT_WORLD);
        // ierr = MatView(SubC,PETSC_VIEWER_DRAW_WORLD); CHKERRQ(ierr);
        // std::cin >> wait;
        // cerr << "Nested SubConjC" << endl;
        // // MatView(SubConjC,PETSC_VIEWER_STDOUT_WORLD);
        // ierr = MatView(SubConjC,PETSC_VIEWER_DRAW_WORLD); CHKERRQ(ierr);
        // std::cin >> wait;
      // }

      nested_matrices(0,0) = SubA;
      nested_matrices(1,1) = SubA;
      nested_matrices(1,0) = SubC;
      nested_matrices(0,1) = SubConjC;

      /* field split */
      // double Anorm,SubAnorm,SubCnorm,SubConjCnorm;
      // ierr = MatNorm(A,NORM_FROBENIUS,&Anorm); CHKERRQ(ierr);
      // ierr = MatNorm(SubA,NORM_FROBENIUS,&SubAnorm); CHKERRQ(ierr);
      // ierr = MatNorm(SubC,NORM_FROBENIUS,&SubCnorm); CHKERRQ(ierr);
      // ierr = MatNorm(SubConjC,NORM_FROBENIUS,&SubConjCnorm); CHKERRQ(ierr);
      // ierr = PetscPrintf(PETSC_COMM_WORLD,"||A|| = %9.8e\n",Anorm);CHKERRQ(ierr);
      // ierr = PetscPrintf(PETSC_COMM_WORLD,"||SubA|| = %9.8e\n",SubAnorm);CHKERRQ(ierr);
      // ierr = PetscPrintf(PETSC_COMM_WORLD,"||SubC|| = %9.8e\n",SubCnorm);CHKERRQ(ierr);
      // ierr = PetscPrintf(PETSC_COMM_WORLD,"||SubConjC|| = %9.8e\n",SubConjCnorm);CHKERRQ(ierr);
      // std::string wait;
      // std::cin >> wait;


      /* get the sparsity information of matrix */
      /* matrix evaluation time */
      ierr = PetscTime(&v2);CHKERRQ(ierr);
      ierr = PetscGetCPUTime(&t2);CHKERRQ(ierr);
      MatInfo info;
      ierr = MatGetInfo(A,MAT_GLOBAL_SUM,&info);
      // if(!pcomm->rank()) {
        // PetscSynchronizedPrintf(PETSC_COMM_WORLD,"info.nz_used %g\n",info.nz_used);
        // PetscSynchronizedPrintf(PETSC_COMM_WORLD,"info.memory %g\n",info.memory/2073741824);
        PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Matrix evaluation total Time = %f S CPU Time = %f S \n",v2-v1,t2-t1);
      // }

      // field split
      // Zero matrix
      Mat B;
      PC pc;
      KSP solver;
      ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);
      // ierr = PCCreate(PETSC_COMM_WORLD,&pc); CHKERRQ(ierr);
      ierr = MatCreateNest(
        PETSC_COMM_WORLD,
        2,&nested_is_rows[0],
        2,&nested_is_rows[0],
        &nested_matrices(0,0),
        &B
      ); CHKERRQ(ierr);
      ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      // ierr = MatDestroy(&A); CHKERRQ(ierr);
      ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
      ierr = KSPSetOperators(solver,B,B); CHKERRQ(ierr);

      // field split
      ierr = KSPGetPC(solver,&pc); CHKERRQ(ierr);
      ierr = PCSetType(pc,PCFIELDSPLIT); CHKERRQ(ierr);
      PetscBool is_pcfs = PETSC_FALSE;
      PetscObjectTypeCompare((PetscObject)pc,PCFIELDSPLIT,&is_pcfs);
      if(is_pcfs) {

        ierr = PCSetOperators(pc,B,B); CHKERRQ(ierr);
        ierr = PCFieldSplitSetIS(pc,NULL,nested_is_rows[0]); CHKERRQ(ierr);
        ierr = PCFieldSplitSetIS(pc,NULL,nested_is_rows[1]); CHKERRQ(ierr);
        // ierr = PCFieldSplitSetType(pc,PC_COMPOSITE_SCHUR); CHKERRQ(ierr);
        // ierr = PCFieldSplitSetType(pc,PC_COMPOSITE_MULTIPLICATIVE); CHKERRQ(ierr);
        ierr = PCSetUp(pc); CHKERRQ(ierr);
        // ierr = PCFieldSplitSetSchurFactType(sub_pc_0,PC_FIELDSPLIT_SCHUR_FACT_LOWER); CHKERRQ(ierr);
        // ierr = PCFieldSplitSetType(sub_pc_0,PC_COMPOSITE_SCHUR); CHKERRQ(ierr);

        // ierr = PCFieldSplitSetSchurPre(pc,PC_FIELDSPLIT_SCHUR_PRE_A11,SubA); CHKERRQ(ierr);
        // KSP *sub_ksp;
        // PetscInt n;
        // ierr = PCFieldSplitGetSubKSP(pc,&n,&sub_ksp); CHKERRQ(ierr);
        // ierr = KSPSetOperators(sub_ksp[1],SubA,SubA);
        // ierr = PetscFree(sub_ksp); CHKERRQ(ierr);

      } else {
        SETERRQ(
          PETSC_COMM_WORLD,
          MOFEM_DATA_INCONSISTENCY,
          "Only works with pre-conditioner PCFIELDSPLIT"
        );
      }

      ierr = VecZeroEntries(F); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

      for(int ss = 0;ss<2;ss++) {
        ierr = VecScatterBegin(
          scatterPressure[ss],rhs_series[ss](f),F,INSERT_VALUES,SCATTER_REVERSE
        ); CHKERRQ(ierr);
        ierr = VecScatterEnd(
          scatterPressure[ss],rhs_series[ss](f),F,INSERT_VALUES,SCATTER_REVERSE
        ); CHKERRQ(ierr);
      }

      // Zero vectors
      ierr = VecZeroEntries(T); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

      /* Normalized the right hand side vector F */
      double nrm2_F;
      ierr = VecNorm(F,NORM_2,&nrm2_F);  CHKERRQ(ierr);
      ierr = ierr = VecScale(F,1/nrm2_F); CHKERRQ(ierr);

      ierr = KSPSetUp(solver); CHKERRQ(ierr);
      // field split

      // Solve problem
      ierr = KSPSolve(solver,F,T); CHKERRQ(ierr);

      /* scale back to normal */
      ierr = ierr = VecScale(T,nrm2_F); CHKERRQ(ierr);


      /* TEST the -nan norm value */
      double fnorm0,fnorm1;
      ierr = VecNorm(T,NORM_2,&fnorm0); CHKERRQ(ierr);
      ierr = VecNorm(F,NORM_2,&fnorm1); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"||T|| , ||F|| = %9.8e\n,%9.8e\n",fnorm0,fnorm1); CHKERRQ(ierr);

      // field split destroy
      ierr = MatDestroy(&B); CHKERRQ(ierr);
      // ierr = KSPDestroy(&solver); CHKERRQ(ierr);
      // ierr = PCDestroy(&pc); CHKERRQ(ierr);

      for(int ss = 0;ss<2;ss++) {
        ierr = VecScatterBegin(
          scatterPressure[ss],T,pSeriesScatterWave[ss](f),INSERT_VALUES,SCATTER_FORWARD
        ); CHKERRQ(ierr);
        ierr = VecScatterEnd(
          scatterPressure[ss],T,pSeriesScatterWave[ss](f),INSERT_VALUES,SCATTER_FORWARD
        ); CHKERRQ(ierr);
      }


    }

    // field split delete entities
    ierr = MatDestroy(&SubA); CHKERRQ(ierr);
    ierr = MatDestroy(&SubC); CHKERRQ(ierr);
    ierr = MatDestroy(&SubConjC); CHKERRQ(ierr);
    for(int i = 0;i!=2;i++) {
      if(nested_is_rows[i]) {
        ierr = ISDestroy(&nested_is_rows[i]); CHKERRQ(ierr);
      }
      for(int j = 0;j!=2;j++) {
        if(nested_matrices(i,j)) {
          ierr = MatDestroy(&nested_matrices(i,j)); CHKERRQ(ierr);
        }
      }
    }

    ierr = destroySeries(rhs_series); CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  /** \brief Apply Forward FFT to data series.

  Note that series is vector of vectors consisting DOFs.

  */
  PetscErrorCode seriesForwardDft(vector<ublas::vector<Vec> > &series,
  bool time_domain_signal) {
    PetscFunctionBegin;

    int n = sSeries.size();
    if(helmholtzElement.globalParameters.nbOfPointsInTime.second) {
      n = helmholtzElement.globalParameters.nbOfPointsInTime.first;
    }
    /* re-instantiate KISS operators */
    complexIn = boost::shared_array<kiss_fft_cpx>(new kiss_fft_cpx[n]);
    complexOut = boost::shared_array<kiss_fft_cpx>(new kiss_fft_cpx[n]);

    // Transform physical time domain to frequency time domain
    int size;
    ierr = VecGetLocalSize(series[0](0),&size); CHKERRQ(ierr);

    /*for(int ss = 0;ss<2;ss++) {
      for(int t = 0;t<n;t++) {
        ierr = VecScale(series[ss](t),1./(double)n); CHKERRQ(ierr);
      }
    }*/

    for(int i = 0;i<size;i++) {
      double *a_real,*a_imag;
      for(int t = 0;t<n;t++) {
        ierr = VecGetArray(series[0](t),&a_real); CHKERRQ(ierr);
        ierr = VecGetArray(series[1](t),&a_imag); CHKERRQ(ierr);
        complexIn[t].r = a_real[i];
        if(time_domain_signal) {
          complexIn[t].i = 0.0;
        } else {
          complexIn[t].i = a_imag[i];
        }
        ierr = VecRestoreArray(series[0](t),&a_real); CHKERRQ(ierr);
        ierr = VecRestoreArray(series[1](t),&a_imag); CHKERRQ(ierr);
      }
      kiss_fft(forwardCfg,complexIn.get(),complexOut.get());
      for(int t= 0;t<n;t++) {
        ierr = VecGetArray(series[0](t),&a_real); CHKERRQ(ierr);
        ierr = VecGetArray(series[1](t),&a_imag); CHKERRQ(ierr);
        a_real[i] = complexOut[t].r;
        a_imag[i] = complexOut[t].i;
        ierr = VecRestoreArray(series[0](t),&a_real); CHKERRQ(ierr);
        ierr = VecRestoreArray(series[1](t),&a_imag); CHKERRQ(ierr);
      }
    }

    PetscFunctionReturn(0);
  }


  /** \brief Apply Inverse FFT to data series.

  */
  PetscErrorCode seriesInverseDft(vector<ublas::vector<Vec> > &series) {
    PetscFunctionBegin;

    int n = sSeries.size();
    if(helmholtzElement.globalParameters.nbOfPointsInTime.second) {
      n = helmholtzElement.globalParameters.nbOfPointsInTime.first;
    }

    inverseCfg = kiss_fft_alloc(n, 1, NULL, NULL);

    int size;
    ierr = VecGetLocalSize(series[0][0],&size); CHKERRQ(ierr);

    for(int ii = 0;ii<size;ii++) {

      for(int k = 0;k<n;k++) {

        double *p_real,*p_imag;
        ierr = VecGetArray(series[0][k],&p_real); CHKERRQ(ierr);
        ierr = VecGetArray(series[1][k],&p_imag); CHKERRQ(ierr);

        complexIn[k].r = p_real[ii];
        complexIn[k].i = p_imag[ii];

        ierr = VecRestoreArray(series[0][k],&p_real); CHKERRQ(ierr);
        ierr = VecRestoreArray(series[1][k],&p_imag); CHKERRQ(ierr);

      }
      kiss_fft(inverseCfg,complexIn.get(),complexOut.get());
      for(int k = 0;k<n;k++) {

        double *a_p;
        ierr = VecGetArray(series[0][k],&a_p); CHKERRQ(ierr);
        a_p[ ii ] = complexOut[k].r;
        ierr = VecRestoreArray(series[0][k],&a_p); CHKERRQ(ierr);

        ierr = VecGetArray(series[1][k],&a_p); CHKERRQ(ierr);
        a_p[ ii ] = complexOut[k].i;
        ierr = VecRestoreArray(series[1][k],&a_p); CHKERRQ(ierr);

      }

    }

    PetscFunctionReturn(0);
  }

  /// Aplly Forward FFT to pressure degrees of freedom
  PetscErrorCode pressureForwardDft() {
    PetscFunctionBegin;
    PetscBool add_incident_wave = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-add_incident_wave",&add_incident_wave,NULL); CHKERRQ(ierr);
    if(add_incident_wave) {
      ierr = seriesForwardDft(pSeriesIncidentWave,true); CHKERRQ(ierr);
    }
    PetscFunctionReturn(0);
  }

  /// Aplly Inverse FFT to pressure degrees of freedom
  PetscErrorCode pressureInverseDft() {
    PetscFunctionBegin;
    PetscBool add_incident_wave = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-add_incident_wave",&add_incident_wave,NULL); CHKERRQ(ierr);
    if(add_incident_wave) {
      ierr = seriesInverseDft(pSeriesIncidentWave); CHKERRQ(ierr);
    }
    ierr = seriesInverseDft(pSeriesScatterWave); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  PetscErrorCode generateReferenceElementMesh() {
    PetscFunctionBegin;
    ierr = postProc.generateReferenceElementMesh(); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  /// Save data on mesh
  PetscErrorCode saveResults() {
    PetscFunctionBegin;

    ierr = postProc.clearOperators(); CHKERRQ(ierr);
    ierr = postProc.addFieldValuesPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);

    /*post-process incident potential */
    Vec p_incident_wave_real;
    Vec p_incident_wave_imag;
    PetscBool add_incident_wave = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-add_incident_wave",&add_incident_wave,NULL); CHKERRQ(ierr);
    if(add_incident_wave) {
      ierr = VecDuplicate(pSeriesIncidentWave[0](0),&p_incident_wave_real); CHKERRQ(ierr);
      ierr = postProc.addFieldValuesPostProc("P","P_INCIDENT_WAVE_REAL",p_incident_wave_real); CHKERRQ(ierr);
      ierr = VecDuplicate(pSeriesIncidentWave[1](0),&p_incident_wave_imag); CHKERRQ(ierr);
      ierr = postProc.addFieldValuesPostProc("P","P_INCIDENT_WAVE_IMAG",p_incident_wave_imag); CHKERRQ(ierr);
    }

    PetscBool pressure_field = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-pressure_field",&pressure_field,NULL); CHKERRQ(ierr);
    PetscBool reynolds_stress = PETSC_FALSE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-reynolds_stress",&reynolds_stress,NULL); CHKERRQ(ierr);

    /*post-process potential */
    Vec p_scatter_wave_real;
    Vec p_pressure_real;
    PetscScalar scaling_pressure;
    ierr = VecDuplicate(pSeriesScatterWave[0](0),&p_scatter_wave_real); CHKERRQ(ierr);
    ierr = postProc.addFieldValuesPostProc("P","P_SCATTER_WAVE_REAL",p_scatter_wave_real); CHKERRQ(ierr);
    ierr = postProc.addFieldValuesGradientPostProc("P","P_SCATTER_VELOCITY_REAL",p_scatter_wave_real); CHKERRQ(ierr);
    if(pressure_field) {
      ierr = VecDuplicate(pSeriesScatterWave[0](0),&p_pressure_real); CHKERRQ(ierr);
      ierr = postProc.addFieldValuesPostProc("P","PRESSURE_REAL",p_pressure_real); CHKERRQ(ierr);
    }

    Vec p_scatter_wave_imag;
    Vec p_pressure_imag;
    ierr = VecDuplicate(pSeriesScatterWave[1](0),&p_scatter_wave_imag); CHKERRQ(ierr);
    ierr = postProc.addFieldValuesPostProc("P","P_SCATTER_WAVE_IMAG",p_scatter_wave_imag); CHKERRQ(ierr);
    ierr = postProc.addFieldValuesGradientPostProc("P","P_SCATTER_VELOCITY_IMAG",p_scatter_wave_imag); CHKERRQ(ierr);
    if(pressure_field) {
      ierr = VecDuplicate(pSeriesScatterWave[1](0),&p_pressure_imag); CHKERRQ(ierr);
      ierr = postProc.addFieldValuesPostProc("P","PRESSURE_IMAG",p_pressure_imag); CHKERRQ(ierr);
    }
    /* FIXME The phase of the solution can be calculated in Paraview using formula arctan(re P/im P) */

    int n = sSeries.size();
    if(helmholtzElement.globalParameters.nbOfPointsInTime.second) {
      n = helmholtzElement.globalParameters.nbOfPointsInTime.first;
    }

    for(int k = 0;k<n;k++) {

      if(add_incident_wave) {
        ierr = VecCopy(pSeriesIncidentWave[0](k),p_incident_wave_real); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(p_incident_wave_real,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(p_incident_wave_real,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecCopy(pSeriesIncidentWave[1](k),p_incident_wave_imag); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(p_incident_wave_imag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(p_incident_wave_imag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      }

      ierr = VecCopy(pSeriesScatterWave[0](k),p_scatter_wave_real); CHKERRQ(ierr);
      if(pressure_field) {
        ierr = VecCopy(pSeriesScatterWave[0](k),p_pressure_real); CHKERRQ(ierr);
        scaling_pressure = 2.0 * M_PI * (k + 1) * helmholtzElement.globalParameters.dEnsity.first;
        ierr = VecScale(p_pressure_real,scaling_pressure);
        ierr = VecGhostUpdateBegin(p_pressure_real,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(p_pressure_real,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      }

      ierr = VecGhostUpdateBegin(p_scatter_wave_real,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(p_scatter_wave_real,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

      ierr = VecCopy(pSeriesScatterWave[1](k),p_scatter_wave_imag); CHKERRQ(ierr);
      if(pressure_field) {
        ierr = VecCopy(pSeriesScatterWave[1](k),p_pressure_imag); CHKERRQ(ierr);
        scaling_pressure *= 1.0;
        ierr = VecScale(p_pressure_imag,scaling_pressure);
        ierr = VecGhostUpdateBegin(p_pressure_imag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(p_pressure_imag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      }

      ierr = VecGhostUpdateBegin(p_scatter_wave_imag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(p_scatter_wave_imag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

      ierr = mField.loop_finite_elements("PRESSURE_IN_TIME","PRESSURE_FE",postProc); CHKERRQ(ierr);

      {
        ostringstream ss;
        ss << "pressure_real_time_step_" << k << ".h5m";
        ierr = postProc.writeFile(ss.str().c_str()); CHKERRQ(ierr);
        PetscPrintf(PETSC_COMM_WORLD,"Saved %s\n",ss.str().c_str());
      }

    }

    if(reynolds_stress) {

      vector<Vec> p_scatter_wave_real(n);
      vector<Vec> p_scatter_wave_imag(n);

      for(int k = 0;k<n;k++) {
        ierr = VecDuplicate(pSeriesScatterWave[0](k),&p_scatter_wave_real[k]); CHKERRQ(ierr);
        ierr = VecCopy(pSeriesScatterWave[0](k),p_scatter_wave_real[k]); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(p_scatter_wave_real[k],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(p_scatter_wave_real[k],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

        ierr = VecDuplicate(pSeriesScatterWave[0](k),&p_scatter_wave_imag[k]); CHKERRQ(ierr);
        ierr = VecCopy(pSeriesScatterWave[0](k),p_scatter_wave_imag[k]); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(p_scatter_wave_imag[k],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(p_scatter_wave_imag[k],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      }

      ierr = postProc.clearOperators(); CHKERRQ(ierr);
      ierr = postProc.addFieldValuesPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);

      for(int k = 0;k<n;k++) {

        helmholtzElement.globalParameters.timeStep = k;
        // string field_grda_name = "P_SCATTER_VELOCITY_REAL_"+string()
        ierr = postProc.addFieldValuesGradientPostProc("P","P_SCATTER_VELOCITY_REAL",p_scatter_wave_real[k]); CHKERRQ(ierr);

        postProc.getOpPtrVector().push_back(
          new ReynoldsStress(
            mField,
            helmholtzElement,
            postProc.postProcMesh,
            postProc.mapGaussPts,
            "P",
            "REAL",
            postProc.commonData
          )
        );

        ierr = postProc.addFieldValuesGradientPostProc("P","P_SCATTER_VELOCITY_IMAG",p_scatter_wave_imag[k]); CHKERRQ(ierr);

        postProc.getOpPtrVector().push_back(
          new ReynoldsStress(
            mField,
            helmholtzElement,
            postProc.postProcMesh,
            postProc.mapGaussPts,
            "P",
            "IMAG",
            postProc.commonData
          )
        );

      }

      ierr = mField.loop_finite_elements("PRESSURE_IN_TIME","PRESSURE_FE",postProc); CHKERRQ(ierr);

      {
        ostringstream ss;
        ss << "reynolds_stress" << ".h5m";
        rval = postProc.postProcMesh.write_file(ss.str().c_str(),"MOAB","PARALLEL=WRITE_PART"); CHKERRQ_MOAB(rval);
        PetscPrintf(PETSC_COMM_WORLD,"Saved %s\n",ss.str().c_str());
      }

      for(int k = 0;k<n;k++) {
        ierr = VecDestroy(&p_scatter_wave_real[k]);
        ierr = VecDestroy(&p_scatter_wave_imag[k]);
      }

    }

    if(add_incident_wave) {
      ierr = VecDestroy(&p_incident_wave_real); CHKERRQ(ierr);
      ierr = VecDestroy(&p_incident_wave_imag); CHKERRQ(ierr);
    }
    ierr = VecDestroy(&p_scatter_wave_real); CHKERRQ(ierr);
    ierr = VecDestroy(&p_scatter_wave_imag); CHKERRQ(ierr);
    if(pressure_field) {
      ierr = VecDestroy(&p_pressure_real); CHKERRQ(ierr);
      ierr = VecDestroy(&p_pressure_imag); CHKERRQ(ierr);
    }
    PetscFunctionReturn(0);
  }

};
