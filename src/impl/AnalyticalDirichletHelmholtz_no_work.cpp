/** \file AnalyticalDirichlethelmholtz.cpp

  Enforce Dirichlet boundary condition for given analytical function,

*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */


#include <MoFEM.hpp>
using namespace MoFEM;
#include <DirichletBC.hpp>
#include <AnalyticalDirichlet.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <AnalyticalDirichletHelmholtz.hpp>

AnalyticalDirichletHelmholtzBC::AnalyticalDirichletHelmholtzBC(MoFEM::Interface& m_field): AnalyticalDirichletBC(MoFEM::Interface& m_field) {};


AnalyticalDirichletBC::DirichletBC_RhsOnly::DirichletBC_RhsOnly(
  MoFEM::Interface& m_field,const string &field,Mat A,Vec X,Vec F
):
AnalyticalDirichletBC::DirichletBC(m_field,field,A,X,F),
tRis_ptr(NULL)
{

}

AnalyticalDirichletHelmholtzBC::DirichletBC_RhsOnly::DirichletBC_RhsOnly(
  MoFEM::Interface& m_field,const string &field
):
AnalyticalDirichletBC::DirichletBC(m_field,field),
tRis_ptr(NULL) {

}

AnalyticalDirichletHelmholtzBC::DirichletBC_LhsOnly::DirichletBC_LhsOnly(
  MoFEM::Interface& m_field,const string &field,Mat A,Vec X,Vec F
):
AnalyticalDirichletBC::DirichletBC(m_field,field,A,X,F),
tRis_ptr(NULL)
{

}

AnalyticalDirichletHelmholtzBC::DirichletBC_LhsOnly::DirichletBC_LhsOnly(
  MoFEM::Interface& m_field,const string &field
):
AnalyticalDirichletBC::DirichletBC(m_field,field),
tRis_ptr(NULL) {

}

PetscErrorCode AnalyticalDirichletHelmholtzBC::DirichletBC_RhsOnly::preProcess() {
  PetscFunctionBegin;

  switch (ts_ctx) {
    case CTX_TSSETIFUNCTION: {
      snes_ctx = CTX_SNESSETFUNCTION;
      snes_x = ts_u;
      snes_f = ts_F;
      break;
    }
    case CTX_TSSETIJACOBIAN: {
      snes_ctx = CTX_SNESSETJACOBIAN;
      snes_B = ts_B;
      break;
    }
    default:
    break;
  }

  ierr = AnalyticalDirichletBC::DirichletBC::iNitalize(); CHKERRQ(ierr);

  if(snes_ctx == CTX_SNESNONE && ts_ctx == CTX_TSNONE) {
    if(dofsIndices.size()>0) {
      ierr = VecSetValues(snes_x,dofsIndices.size(),&dofsIndices[0],&dofsValues[0],INSERT_VALUES); CHKERRQ(ierr);
    }
    ierr = VecAssemblyBegin(snes_x); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(snes_x); CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode AnalyticalDirichletHelmholtzBC::DirichletBC_RhsOnly::postProcess() {
  PetscFunctionBegin;

  switch (ts_ctx) {
    case CTX_TSSETIFUNCTION: {
      snes_ctx = CTX_SNESSETFUNCTION;
      snes_x = ts_u;
      snes_f = ts_F;
      break;
    }
    case CTX_TSSETIJACOBIAN: {
      snes_ctx = CTX_SNESSETJACOBIAN;
      snes_B = ts_B;
      break;
    }
    default:
    break;
  }

  if(snes_ctx == CTX_SNESNONE && ts_ctx == CTX_TSNONE) {
    ierr = VecAssemblyBegin(snes_f); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(snes_f); CHKERRQ(ierr);
    for(vector<int>::iterator vit = dofsIndices.begin();vit!=dofsIndices.end();vit++) {
      ierr = VecSetValue(snes_f,*vit,0,INSERT_VALUES); CHKERRQ(ierr);
    }
    ierr = VecAssemblyBegin(snes_f); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(snes_f); CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode AnalyticalDirichletHelmholtzBC::DirichletBC_LhsOnly::postProcess() {
  PetscFunctionBegin;

  switch (ts_ctx) {
    case CTX_TSSETIFUNCTION: {
      snes_ctx = CTX_SNESSETFUNCTION;
      snes_x = ts_u;
      snes_f = ts_F;
      break;
    }
    case CTX_TSSETIJACOBIAN: {
      snes_ctx = CTX_SNESSETJACOBIAN;
      snes_B = ts_B;
      break;
    }
    default:
    break;
  }

  ierr = DirichletBC::iNitalize(); CHKERRQ(ierr);

  if(snes_ctx == CTX_SNESNONE && ts_ctx == CTX_TSNONE) {
    ierr = MatAssemblyBegin(snes_B,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(snes_B,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatZeroRowsColumns(snes_B,dofsIndices.size(),&dofsIndices[0],dIag,PETSC_NULL,PETSC_NULL); CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);

}

PetscErrorCode AnalyticalDirichletBC::sortBc(
  DirichletBC &bc,Range &tris
) {
  PetscFunctionBegin;
  PetscErrorCode ierr;
  bc.tRis_ptr = &tris;
  bc.map_zero_rows.clear();
  bc.dofsIndices.clear();
  bc.dofsValues.clear();
  PetscFunctionReturn(0);

}
