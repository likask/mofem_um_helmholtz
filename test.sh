
rm -rf CPU_times_droplet_p.txt

echo LOG: test the actuation of droplet computational times analysis test
echo | tee -a CPU_times_droplet_p.txt
echo "Start droplet calculation time..." | tee -a CPU_times_droplet_p.txt

mpirun -np 6 ./fe_approximation \
-my_file dense_mix_droplet.cub \
-my_is_partitioned false \
-ksp_type minres \
-ksp_final_residual \
-ksp_monitor \
-ksp_converged_reason \
-pc_type cholesky \
-ksp_monitor_true_residual \
-pc_factor_mat_solver_package mumps \
-my_order 1 \
-save_postproc_mesh true \
-wave_number 8325.2 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave false \
-wave_direction 1,0,0  \
-space_data acoustic_data.txt \
-monochromatic_wave false  \
-signal_length 0.00075472 \
-signal_duration 0.00000018868 \
-mat_mumps_icntl_14 100 \
-radiation_field false \
-frequency 5300000 \
-velocity 1500 \
-complex_wave_number 2768 \
-rayleigh_wave false \
-pressure_field false \
-nb_of_time_step 10 \
-density 999 \
-wave_oscilation_direction 0.9650,0.001,0.1 \
-reynolds_stress true \
-radiation_force true |
grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i CPU_times_droplet_p.txt
echo "   "
#-radiation_force true 2>&1 |
