#mpirun -np 9 ./fe_approximation \
#-my_file analytical_solution.h5m \
#-my_is_partitioned false \
#-wave_number 3 \
#-wave_direction 1,0,0 \
#-save_postproc_mesh false \
#-pc_factor_mat_solver_package mumps \
#-ksp_monitor \
#-my_order 8 \
#-pc_type lu \
#-ksp_type fgmres \
#-my_max_post_proc_ref_level 0 \
#-amplitude_of_incident_wave 1 \
#-duffy true \
#-lobatto true \
#-adaptivity false \
#-add_incident_wave false 2>&1
#ODISP="1"
#ORDER="2"
#if [ "$ORDER" -gt "$ODISP" ]
#then echo "god order"
#else echo "it is odisp"
#fi

WAVE_NUMBER="3"

ODISP="3"
KKK="1"
SOLVER="mumps"
BEST_NB_PRC="8"
# example="kick"


NB_PROC="8"
ERROR_TYPE="l2"
PRIORI_TYPE="1"
ORDER="4"
b="3"
COUNT="0"
# echo $ORDER'>'$4 | bc -l
#

# if [ "echo $ORDER'>'$4 | bc -l" -eq "1" ]
# then echo $ORDER is greater than 4
# else echo $ORDER is smaller than 4
# fi
for COUNT in "1" "2" "3" "4" "5"
do
COUNT=`expr $COUNT + 1`
echo COUNT = $COUNT
if [ "$COUNT" -gt "4" ]
then NB_PROC=`expr 8`
else NB_PROC=`expr 2`
fi
echo we have $NB_PROC
done



for ERROR_TYPE in "l2" "h1"
do
echo "ERROR_TYPE = $ERROR_TYPE"
if [ $ERROR_TYPE = "h1" ]
then PRIORI_TYPE="2"
fi
echo "PRIORI_TYPE = $PRIORI_TYPE"

done

echo "ACOUSTIC_PROBLEM cols 1"
echo "ACOUSTIC_PROBLEM cols 2"



# echo "ACOUSTIC_PROBLEM cols ACOUSTIC_PROBLEM cols" | example=$(grep --line-buffered -i "ACOUSTIC_PROBLEM" | uniq)
# grep only affects line just above its commands,
echo "gogo"
echo "gogo"
# echo "ACOUSTIC_PROBLEM cols 3" | echo $(grep --line-buffered -i "ACOUSTIC_PROBLEM" | sort | uniq) | echo $(grep --line-buffered -i "ACOUSTIC_PROBLEM cols" | sort | uniq) |
# if grep --line-buffered -i "ACOUSTIC_PROBLEM cols"; then
#     echo found
# else
#     echo not found
# fi


echo "ACOUSTIC_PROBLEM cols 3" |
if grep --line-buffered -i "ACOUSTIC_PROBLEM cols"; then
    echo found
else
    echo not found
fi

# for mLine in `grep -n 'COUSTIC_PROBLEM'`
# do
#   echo 'mLine = '${mLine}
# done
# echo $example
# if [echo $(grep --line-buffered -i "ACOUSTIC_PROBLEM cols" | sort | uniq) = "ACOUSTIC_PROBLEM cols"]
# then echo "ACOUSTIC_PROBLEM cols is true"
# fi
# echo "Cao 1 2 3" |
# if [$(grep --line-buffered -i "ACOUSTIC_PROBLEM cols" | sort | uniq) = "ACOUSTIC_PROBLEM cols"]
# then echo "ACOUSTIC_PROBLEM cols is true"
# fi

#
#
# for SOLVER in "mumps" "batman" "superman"
# do
#  echo $SOLVER
# done
#
# for WAVE_NUMBER in "3" "5" "10"
# do
#
# for ODISP in "3" "2"
# do
#
# BEST_NB_PRC=`expr 2 + $(echo $(( 2 * $ODISP )))`
# echo $BEST_NB_PRC
# # echo impinging_cylinder_${ODISP}.cub
# mpirun -np $BEST_NB_PRC ./best_approximation \
# -my_file impinging_cylinder_${ODISP}.cub \
# -my_is_partitioned false \
# -wave_number $WAVE_NUMBER \
# -wave_direction 1,0,0 \
# -analytical_solution_type hard_cylinder_scatter_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 8 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -lobatto true
#
# done
# done
