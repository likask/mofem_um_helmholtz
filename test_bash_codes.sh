# # #
# # #
# # # ODISP="4"
# # #
# # # ~/build_moFEM1/usr/tools/mofem_part -my_file impinging_cylinder_$ODISP.cub -my_nparts 8
# # #
# # # echo "Start numerical calculation field split is $FIELD_SPLIT !!! with $NB_PROC processes, mesh h $ODISP and order p 2 ..."
# # # mpirun -np 8 ./fe_field_split \
# # # -my_file out.h5m \
# # # -my_is_partitioned true \
# # # -wave_number 10 \
# # # -wave_direction 1,0,0 \
# # # -save_postproc_mesh false \
# # # -my_order 4 \
# # # -my_max_post_proc_ref_level 0 \
# # # -amplitude_of_incident_wave 1 \
# # # -duffy true \
# # # -lobatto true \
# # # -fieldsplit_1_ksp_type fgmres \
# # # -fieldsplit_1_pc_type lu \
# # # -fieldsplit_1_pc_factor_mat_solver_package superlu_dist \
# # # -fieldsplit_1_ksp_max_it 30 \
# # # -fieldsplit_0_ksp_type fgmres \
# # # -fieldsplit_0_pc_type lu \
# # # -fieldsplit_0_pc_factor_mat_solver_package superlu_dist \
# # # -fieldsplit_0_ksp_max_it 30 \
# # # -pc_fieldsplit_type schur \
# # # -ksp_type fgmres \
# # # -ksp_atol 1e-8 \
# # # -ksp_rtol 1e-8 \
# # # -ksp_max_it 800 \
# # # -ksp_monitor \
# # # -add_incident_wave false 2>&1 |
# # # # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# # # # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$4,$6,$8,$10,$15,$25,$32,$37}' | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# # # # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$15,$22,$23,$27,$28}' | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# # # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$5,$9,$16,$21}' | tee -a -i hard_cylinder_field_split_true_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# # # # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$29}' | tee -a -i hard_cylinder_field_split_true_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# # # # echo "   "
# #
# #
# #
# #
# #
# #
# #
# # # run 10MHZ droplet problem, and impinging sphere problem in time domain
# #
# #
# # #
# # #
# # # # remove all the previous time dependant results.
# # rm -rf pressure_real_time_step_*.h5m
# # #
# #
# # # rm -rf droplet_pressure
# # #
# # #
# ~/build_moFEM1/usr/tools/mofem_part -my_file ./mix_droplet.cub -my_nparts 8
# # #
# # # # 4 MHz
# # # # mpirun -np 8 ./fe_approximation \
# # # # -my_file out.h5m \
# # # # -my_is_partitioned true \
# # # # -duffy true \
# # # # -adaptivit false \
# # # # -error_level 1 \
# # # # -my_order 6 \
# # # # -save_postproc_mesh true \
# # # # -wave_number 6283.1 \
# # # # -my_max_post_proc_ref_level 2 \
# # # # -add_incident_wave true \
# # # # -wave_direction 1,0,0 \
# # # # -wave_oscilation_direction -0.3756,-0.9268,0.000000001 \
# # # # -monochromatic_wave false \
# # # # -signal_length 0.001 \
# # # # -radiation_field true \
# # # # -frequency 4000000 \
# # # # -signal_duration 0.00000025 \
# # # # -velocity 1500 \
# # # # # -complex_wave_number 79.8387 \
# # # # -complex_wave_number 2768 \
# # # # -transmission_coefficient 0.1491 \
# # # # -rayleigh_wave false \
# # # # -density 998 \
# # # # -reynolds_stress true \
# # # # -radiation_force true \
# # # # -material_coefficient1 -1348.8  \
# # # # -ksp_atol 1e-8 \
# # # # -ksp_rtol 1e-8 \
# # # # -ksp_max_it 2000 \
# # # # -ksp_monitor \
# # # # -space_data acoustic_data.txt \
# # # # -nb_of_time_step 14 \
# # # # -material_coefficient2  \
# # # # -ksp_converged_reason \
# # # # -ksp_monitor_true_residual \
# # # # -pc_type lu \
# # # # -ksp_type fgmres \
# # # # -pc_factor_mat_solver_package superlu_dist \
# # # # -droplet_radius 0.00210 \
# # # # -attenuation 0.067077277922418
# # #
# # #
# mpirun -np 8 ./fe_approximation \
# -my_file out.h5m \
# -my_is_partitioned true \
# -ksp_final_residual \
# -ksp_monitor \
# -ksp_converged_reason \
# -pc_type lu \
# -ksp_type fgmres \
# -pc_factor_mat_solver_package superlu_dist \
# -duffy true \
# -adaptivit false \
# -error_level 1 \
# -my_order 6 \
# -save_postproc_mesh true \
# -wave_number 15707.96326794897 \
# -my_max_post_proc_ref_level 2 \
# -add_incident_wave true \
# -wave_direction 1,0,0 \
# -wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
# -monochromatic_wave false \
# -signal_length 0.0004 \
# -signal_duration 0.0000001 \
# -frequency 10000000 \
# -velocity 1500 \
# -complex_wave_number 2768 \
# -transmission_coefficient 0.1491 \
# -rayleigh_wave false \
# -density 998 \
# -droplet_radius 0.00210 \
# -attenuation 0.265146221848709 \
# -reynolds_stress true \
# -radiation_force true \
# -material_coefficient1 -1348.8 \
# -ksp_atol 1e-8 \
# -ksp_rtol 1e-8 \
# -ksp_max_it 2000 \
# -space_data acoustic_data.txt \
# -nb_of_time_step 13 \
# -material_coefficient2 | tee log_droplet
# # #
# # #
# # # ./do_vtk1.sh pressure_real_time_step_*.h5m
# # # mbconvert reynolds_stress.h5m reynolds_stress.vtk
# # # mkdir droplet_pressure
# # # mv pressure_real_time_step_*.vtk droplet_pressure
# # # mv reynolds_stress.vtk droplet_pressure
# #
# #
# # # -complex_wave_number 199.5968 \
# #
# #
# # rm -rf sphere_pressure
# #
# # ~/build_moFEM1/usr/tools/mofem_part -my_file ./impinging_sphere.cub -my_nparts 8
# #
# #
# #
# #
# #
# # mpirun -np 8 ./fe_field_split \
# # -my_file out.h5m \
# # -my_is_partitioned true \
# # -duffy true \
# # -adaptivit false \
# # -error_level 1 \
# # -my_order 6 \
# # -save_postproc_mesh true \
# # -wave_number 5 \
# # -my_max_post_proc_ref_level 2 \
# # -add_incident_wave true \
# # -wave_direction 1,0,0  \
# # -monochromatic_wave false \
# # -space_data acoustic_data.txt \
# # -nb_of_time_step 12 \
# # -signal_length 1.2566 \
# # -signal_duration 0.0008377580409572781 \
# # -amplitude_of_incident_wave 1 \
# # -fieldsplit_1_ksp_type fgmres \
# # -fieldsplit_1_pc_type lu \
# # -fieldsplit_1_pc_factor_mat_solver_package mumps \
# # -fieldsplit_1_ksp_max_it 45 \
# # -fieldsplit_0_ksp_type fgmres \
# # -fieldsplit_0_pc_type lu \
# # -fieldsplit_0_pc_factor_mat_solver_package mumps \
# # -fieldsplit_0_ksp_max_it 45 \
# # -pc_fieldsplit_type schur \
# # -ksp_type fgmres \
# # -ksp_atol 1e-8 \
# # -ksp_rtol 1e-8 \
# # -ksp_max_it 800 \
# # -ksp_monitor \
# # -density 998 \
# # -reynolds_stress true | tee log_sphere
# #
# # # -wave_number 10 \
# # # -signal_length 0.6283 \
# # # -signal_duration 0.0004233827493261457 \
# #
# #
# # ./do_vtk1.sh pressure_real_time_step_*.h5m
# # mbconvert reynolds_stress.h5m reynolds_stress.vtk
# # mkdir sphere_pressure
# # mv reynolds_stress.vtk sphere_pressure
# # mv pressure_real_time_step_*.vtk sphere_pressure
# #
# #
# #
# #
# #
# #
# #
# #
# #
# #
# # # END
#
# # mbpart -t -p PARTKWAY 8 mix_droplet.cub mix_droplet_8.h5m
# #
# # ############### START UNIFORM ADAPTIVITY calculation ####################
# # ADAPTIVITY="false"
# #
# echo "Start uniform P enrichment numerical calculation with $NB_PROC processes, P Non-uniform ? ${ADAPTIVITY}.  mesh mix_droplet.cub and order  7 ($COUNT) and wavenumber $WAVE_NUMBER, error level $ERRORLV"
# mpirun -np 8 ./fe_approximation \
# -my_file mix_droplet_8.h5m \
# -my_is_partitioned true \
# -wave_number 5 \
# -wave_direction 1,0,0 \
# -wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
# -material_coefficient1 -1348.8 \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package mumps \
# -ksp_monitor \
# -my_order 3 \
# -my_max_post_proc_ref_level 0 \
# -amplitude_of_incident_wave 1 \
# -duffy true \
# -lobatto true \
# -adaptivity false \
# -error_level 2 \
# -error_type 1 \
# -add_incident_wave false 2>&1 |
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq)
#  # | awk '{print $2,$9,$16,$21}'
# # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$29}' | tee -a -i p_refinement_adaptivity_${ADAPTIVITY}_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#
# echo "   "
# # echo "                       begin error calculation           "
# echo " check if data is correct  "
# #echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$25,$29,$30}' | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# #echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# #BEGIN error calculation   awk '{print $6,$12}'  grep first 10 lines |head -10|
# #grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#
# # # mpirun -np 2 ./error_norm \
# # # -my_file ./fe_solution.h5m \
# # # -my_is_partitioned false \
# # # -norm_type $ERROR_TYPE \
# # # -relative_error false \
# # # -ksp_type fgmres \
# # # -pc_type lu \
# # # -pc_factor_mat_solver_package superlu_dist \
# # # -ksp_monitor \
# # # -my_order 1 \
# # # -my_max_post_proc_ref_level 0 \
# # # -save_postproc_mesh false 2>&1 | echo $(grep --line-buffered -i " realtive error ") | awk '{print $6,$12}' | tee -a -i p_refinement_adaptivity_${ADAPTIVITY}_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# # #
#
#
# # mbpart -t -p PARTKWAY 8 dense_droplet.cub dense_droplet.h5m
# #
# #
# #
# #
# # echo "Start H refinement numerical calculation with $NB_PROC processes, order = 2,  H Refinement mesh dense_droplet.h5m and order 2 and wavenumber $WAVE_NUMBER, error level $ERRORLV"
# # mpirun -np 8 ./fe_approximation \
# # -my_file dense_droplet.h5m \
# # -my_is_partitioned true \
# # -wave_number 5 \
# # -wave_direction 1,0,0 \
# # -wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
# # -material_coefficient1 -1348.8 \
# # -save_postproc_mesh false \
# # -ksp_type fgmres \
# # -pc_type lu \
# # -pc_factor_mat_solver_package mumps \
# # -ksp_monitor \
# # -my_order 2 \
# # -my_max_post_proc_ref_level 0 \
# # -amplitude_of_incident_wave 1 \
# # -duffy true \
# # -lobatto true \
# # -adaptivity false \
# # -add_incident_wave false 2>&1 |
# # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq)
# # # echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$29}' | tee -a -i h_refinement_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# # # echo "   "
# # # echo "                       begin error calculation           "
# # echo " check if data is correct  "
#
#
#
#
# mpirun -np 8 ./fe_approximation -my_file mix_droplet.cub -my_is_partitioned false -wave_number 6283.185307179586 -wave_direction 1,0,0 -wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 -material_coefficient1 -1348.8 -save_postproc_mesh true -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -my_order 8 -my_max_post_proc_ref_level 2 -amplitude_of_incident_wave 1 -duffy true -lobatto true -adaptivity true -error_level 2 -error_type 2 -add_incident_wave false -complex_wave_number 79.8387 -frequency 4000000 -velocity 1500

rm -rf fe_solution_mesh_post_proc_adaptivity_10mhz.vtk

mpirun -np 8 ./fe_approximation \
-my_file mix_droplet.cub \
-my_is_partitioned false \
-wave_number 15707.96326794896 \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-material_coefficient1 -1348.8 \
-save_postproc_mesh true \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package mumps \
-ksp_monitor \
-my_order 1 \
-my_max_post_proc_ref_level 3 \
-amplitude_of_incident_wave 1 \
-duffy true \
-lobatto true \
-adaptivity true \
-error_level 2 \
-error_type 2 \
-add_incident_wave false \
-frequency 10000000 \
-velocity 1500 \
-complex_wave_number 199.5968

mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mv fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_adaptivity_10mhz.vtk




rm -rf fe_solution_mesh_post_proc_p_10mhz.vtk
~/build_moFEM1/usr/tools/mofem_part -my_file ./mix_droplet.cub -my_nparts 8

mpirun -np 8 ./fe_approximation \
-my_file out.h5m \
-my_is_partitioned true \
-wave_number 15707.96326794896 \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-material_coefficient1 -1348.8 \
-save_postproc_mesh true \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package mumps \
-ksp_monitor \
-my_order 8 \
-my_max_post_proc_ref_level 3 \
-amplitude_of_incident_wave 1 \
-duffy true \
-lobatto true \
-adaptivity false \
-error_level 2 \
-error_type 2 \
-add_incident_wave true \
-frequency 10000000 \
-velocity 1500 \
-complex_wave_number 199.5968

mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mv fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_p_10mhz.vtk


rm -rf fe_solution_mesh_post_proc_h_10mhz.vtk
~/build_moFEM1/usr/tools/mofem_part -my_file ./dense_droplet.cub -my_nparts 8

mpirun -np 8 ./fe_approximation \
-my_file out.h5m \
-my_is_partitioned true \
-wave_number 15707.96326794896 \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-material_coefficient1 -1348.8 \
-save_postproc_mesh true \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package mumps \
-ksp_monitor \
-my_order 2 \
-my_max_post_proc_ref_level 3 \
-amplitude_of_incident_wave 1 \
-duffy true \
-lobatto true \
-adaptivity false \
-error_level 2 \
-error_type l2 \
-add_incident_wave false \
-frequency 10000000 \
-velocity 1500 \
-complex_wave_number 199.5968

mbconvert fe_solution_mesh_post_proc.h5m fe_solution_mesh_post_proc.vtk
mv fe_solution_mesh_post_proc.vtk fe_solution_mesh_post_proc_h_10mhz.vtk



#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# ### END ###
